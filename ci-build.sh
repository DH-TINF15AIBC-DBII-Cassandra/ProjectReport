#!/bin/bash

(echo --- && cat templates/settings.yml && echo ---) > _combined.md
for chapter in $(cat toc.txt); do 
	(echo && echo && cat chapters/$chapter) >> _combined.md
done

token=$(python3 mendeley_api.py)
  
curl 'https://api.mendeley.com/documents?view=bib&group_id=c6634ae0-3931-37cf-908b-4794ad65b11c' -H 'Authorization: Bearer $token' -H 'Accept: application/x-bibtex' > DW_Cassandra_Spark.bib
pandoc --from=markdown+hard_line_breaks+backtick_code_blocks+abbreviations --top-level-division=chapter --template=./templates/eisvogel.tex --pdf-engine=lualatex --filter=pandoc-crossref --filter=pandoc-citeproc --listings _combined.md -o ProjectReport.pdf 