[sparklogo]: images/spark-logo-trademark.png
[sparkhadooptrend]: images/spark_hadoop_trend.png
[legende]: images/legende.png
[hadooparchitecture]: images/Hadoop_architecture.png
[lazytransofrmaction]: images/lazy_transoform_action.png
[mapreduce]: images/map_reduce_allgemien.png
[sparklibs]: images/sparklibs.png
[wordcountmapreduce]: images/WordCountMapReduce.png	"h"
[sparkonhadoop]: images/spark_on_hadoop.png
[lazyeval]: images/Advantages-of-Lazy-Evaluation-01.png
[DAG]: images/lazy_transform_action.png
# Apache Spark {#sec:cpt-spark}

Apache Spark ist ein Open-Source Framework zur verteilten Verarbeitung großer Datenmengen. Dabei werden die Daten größtenteils im Arbeitspeicher vorgehalten (engl.: in-memory data processing). Die Einsatzmöglichkeiten umfassen unter anderem Data Mining, Machine Learning, Graph Processing, Batch- und Streamverarbeitung. Ein breites Spektrum an APIs steht zur Verfügung, welches die für die Programmiersprachen Scala, Python, Java und R high-level Programmierschnittstellen bietet.

Das Framework wurde 2010 als Forschungsprojekt der University of California in Berkeley entwickelt [@Zaharia] und ist unter einer Open-Source-Lizenz öffentlich verfügbar. Seit 2013 wird das Projekt von der Apache Software Foundation weitergeführt. 

## Verwendung
Das Apache Spark Framework steigt in den letzten Jahren stetig an Beliebtheit. Es findet Anwendung in Projekten vieler großer Organisationen, wie eBay, SAP (Leonardo) oder IBM [@ApacheSoftwareFoundation]. 
In der Vergangenheit war das **Apache Hadoop Framework** führend im Bereich der verteilten Datenverarbeitung. Interesse an Apache Spark stieg in den letzten Jahren [@GoogleTrends].

![Trend in der Online Suche von Apache Spark im Vergleich mit Apache Hadoop [@GoogleTrends]][sparkhadooptrend]

Apache Spark ist jedoch nicht in allen Fällen als bessere Alternative des Apache Hadoop Frameworks zu sehen. Um die Funktionsweise von Spark zu verstehen und die Unterschiede und Vorteile gegenüber Hadoop zu erkennen, werden im Folgenden die Grundkonzepte der beiden Technologien erklärt: 

Apache Spark ist historisch aus vorherigen Technologien von Apache Hadoop entstanden. Aus diesem Grund wird zuerst die Funktionsweise von Apache Hadoop erläutert. Dabei wird das **HDFS (Hadoop Distributed File System)** und **Yarn (Yet Another Resource Negotiator)** kurz vorgestellt. Ins besondere wird dann **Apache Hadoop MapReduce** als Methode der verteilten Berechnung genauer eingegangen, da Apache Spark als Alternative des MapReduce Algorithmus  gesehen werden kann.
Auf der Basis dieses Wissens wird in den darauf folgenden Kapiteln dann gezeigt, durch welche Merkmale sich Spark hervorhebt und wann es Hadoop MapReduce übertrifft. Die Vorteile der Nutzung werden erläutert.

## Apache Hadoop

Dieses Kapitel erklärt die Grundkonzepte von Apache Hadoop, um die Motivation und Entscheidung für die Architektur und Methoden von Apache Spark besser zu verstehen. Dabei wird insbesondere auf den MapReduce Algorithmus eingegangen.

### Architektur

Das Apache Hadoop Framework besteht aus verschiedenen Komponenten. Die wichtigsten können eingeordnet werden in die Kategorien Persistierung und Zugriff (**HDFS** + **Yarn**) und die Prozessierung (**MapReduce**) von Daten. Yarn  ermöglicht in Kombination mit dem zugrundeliegenden HDFS Layer (ab Hadoop Version 2.x) die die verschiedenen Formen der Datenverarbeitung, wie  iterative Verarbeitung, Graph Processing, Batch- und Streamverarbeitung und Verarbeitung der Daten aus dem HDFS [@ShwatiKumar2017]. Neben der Resourcenverwaltung wird Yarn auch zum Job Scheduling genutzt. Die Einzelheiten der Persistierung und Nutzung von Daten durch Yarn und HDFS wird in dieser Arbeit nicht weiter dargestellt.

![Hadoop Architektur[@SoftwebSolutions2015]][hadooparchitecture]

Das **Hadoop Distributed File System** (**HDFS**) bietet eine verteilte Speicherung von Daten. Daten werden dabei auf verschiedenen Computern (Knoten) in einem Rechnerverbund (eng. computer cluster) persistiert. 

Der **MapReduce** Algorithmus ermöglicht die Ausführung von Berechnungen, in welchen die Arbeitslast auf einen Rechnerverbund aufgeteilt wird. Dabei nutzt MapReduce die Eigenschaft des unterliegenden HDFS. Daten, welche auf einem gewissen Daten-Knoten im Cluster persistiert sind, werden auch nahe diesem Knoten verarbeitet. Die Abbildung weiter oben stellt dieses Konzept anschaulich dar (Apache Hadoop 1.x) [@Opensource.com, @Almeida2015]. Die genaue Funktionsweise von MapReduce wird im Folgenden erläutert.

### MapReduce

MapReduce ist eine Methode um Probleme zu lösen, welche von einer starke Paralleisierung der Verarbeitungsprozesse profitieren können. Die Methode macht sich die verteilte Speicherung von Daten zum Vorteil, indem es Berechnungen auf Daten nahe dem entsprechenden Daten-Knoten durchführt (wie in der Architektur zu sehen). Knoten, welche diese datennahe Berechnungen durchführen werden auch *Worker* (oder *Worker Node*) genannt. Ein *Master* (oder *Master Node*) koordiniert dabei die verteilte Arbeit der Worker.

Der MapReduce Algorithmus besteht aus 3 Phasen:

- **Map**:

  Jeder Worker wendet eine "*map()*" Funktion auf die Daten an, welche ihm lokal zur Verfügung stehen. Bei dieser Funktion entstehen *Key-Value-Paare*. Diese werden temporär wieder in den Speicher des Daten-Knotens geschrieben. Dies kann mit allen Workern parallel erfolgen.

- **Shuffle**:

  Worker Nodes verteilen die Daten (jetzt Key-Value-Paare) unter sich neu. Dabei werden Daten mit dem gleichen *Key* auf den gleichen Worker veteilt.

- **Reduce**:

  Die Worker verarbeiten nun die enstandenen Gruppen (gruppiert nach Key) an Daten und liefern für jeden Key ein Ergebniss. Diese Einzelergebnisse werden vom Master Node gesammelt und bilden das Endergebniss des Algorithmus.

In der Abbildung ist der Ablauf von MapReduce zu sehen [@Jeffrey2010, @Deana].

![MapReduce im Allgemeinen][mapreduce]

Um den Algorithmus besser zu verstehen ist in der folgenden Abbildung ein Beispiel gegeben. Das Ziel ist es die Worte eines Textes zu zählen. 

![MapReduce Beispiel[@GaoChao2017]][wordcountmapreduce]

Der ursprüngliche Text (**Input**) wird durch **Splitting** in dem Cluster verteilt. Jeder Worker weißt jedem Wort des lokalen Speichers mittels eines **Mapping** den Wert 1 zu. Dadurch entsteht das Key-Value-Paar `<Wort,1>`. Durch **Shuffeling** werden die Key-Value-Paare anhand ihres Keys auf den Knoten gruppiert. Der **Reducing**  Schritt addiert in diesem Fall einfach alle Values eines Keys. Das Endergebniss (**Final Result**) wird vom master node gesammelt und kann präsentiert werden.

Apache Spark hat eine ähnliche Funktionsweise. Jedoch gibt es wichtige Unterschiede, welche Spark einen Vorteil gegenüber MapReduce bieten können. Diese werden im nächsten Kapitel vorgestellt.



## Apache Spark

Apache Spark, als Framework zur verteilten Datenverarbeitung, steigt in Beliebtheit und wird in vielen Systemen verwendet. Das Framework basiert dabei auf einem Algorithmus zur verteilen Berechnung, welche dem zuvor erklärten MapReduce ähnlich ist. Wie in der Abbildung zu sehen, basiert auch Spark auf Woker Nodes, welche in Nähe zu einem Daten-Knoten Berechnungen durchführen. Sogenannten **Executers** auf einem Worker Node werden dabei von der zentralen Verwaltungsinstanz (***Master/Client Node***) *Tasks* kontrolliert.

![Spark auf HDFS][sparkonhadoop]

Trotz oberflächlicher Ähnlichkeiten in der grundlegenden Funktionsweise zu Hadoop MapReduce, gelingt es Spark in vielen Fällen entscheidende Vorteile gegenüber MapReduce zu bieten:

###### Performance

Spark zeigt immer wieder Stärken in Performance und Geschwindigkeit. Vor allem im Vergeich zu Hadoop's MapReduce werden deutliche Unterschiede in der Performance offensichtlich.

Bereits 2014 bewies Spark bei einem Test im Rahmen des *Gray Sort Benchmarks (Daytona 100TB Kategorie)* seine Überlegenheit. Beim Sortieren von 100TB an Daten benötigte es nur 23 Minuten im Vergleich zum vorherigen Rekord von 72 Minuten (durch ein Hadoop MapReduce Cluster) [@Databricks, @Xin2014].

Die gute Performance wird vor allem durch in-memory computing erreicht. Daten werden dabei nicht während des Arbeitsprozesses in den Speicher zurückgeschrieben, sondern im Arbeitsspeicher vorbehalten. 

Somit eigenet sich Spark's in-memory computing vor allem für iterative Berechnungen, welche mehrfach die selben Daten nutzen. Der Grund dafür ist, dass Zwischenergebnisse iterativer Berechnung nicht in den Hauptspeicher abgelegt werden bevor sie in der nächsten Iteration wieder bennötigt werden. Ein Beispiel dafür ist die Erstellung eines linear regression models (aus dem Bereich des Supervised Machine Learnings). 

In der folgenden Abbildung wird das Trainieren eines logistic regression models dargestellt. Die benötigte Zeit im Vergleich zu Hadoop's MapReduce in Korrelation mit der Anzahl der Iterationen ist gezeigt [@Liang2015]. 

![Vergleich zwischen Hadoop MapReduce und Spark [@Liang2015]](images/spark_v_hadoop.png){#fig:vergleich-hadoop width=70%}

###### Flexibilität

Spark bietet durch batch-orientierte, iterative oder Streaming-Analysen ein breites Spektrum an Nutzungsmöglichkeiten. "Sparks flexible Nutzungsbreite bedeutet, dass bestehende Big-Data-Anwendungen schneller und differenzierter betrieben werden können", sagt Reynold Xin, Daten-Engineer und Mitgründer von Databricks - also dem Unternehmen, das das Apache-Spark-Projekt maßgeblich gestaltet [@Weiss2015]. 

###### Zusätzliche Funktionalitäten {#sec:cpt-spark-zus}

Intergriert in das Framework sind viele weitere Features, welche das Arbeiten mit Spark vereinfachen und Berreichern. Neben vielen *Third Party Projects*, bieten offizielle Spark Libraries unter anderem folgenden Möglichkeiten:

  - Echtzeitverarbeitung
  - Graphverarbeitung
  - Machine Learning
  - SQL  

Die entsprechenden Funktionsbibliotheken werden in einem späteren Kapitel näher beleuchted.


Obwohl Apache Spark viele Vorteile bietet, hat auch Apache Hadoop Vorteile: Spark hält so viele Daten wie möglich im Arbeitspeicher vor. Dadurch ergibt sich ein sehr hoher Bedarf an Arbeitsspeicher. Hadoop MapReduce beendet Prozesse sobald ein Job beendet wurde und schreibt Daten wieder in den Speicher. Somit ist das Verarbeiten von sehr großen Datenmengen bei linearer Verarbeitung (im Gegensatz zu iterativer Prozessierung) in manchen Fällen günsitger durch Apache Hadoop.


Patrick McFadin von DataStax fasst den Unterschied zu Hadoop so zusammen: "Hadoop ist der Standard, wenn es um Data-Warehouse und Offline-Datenanalysen geht, doch Spark mit Cassandra ist eine bessere Alternative für alle Anwendungen, bei denen die Geschwindigkeit eine große Rolle spielt - beispielsweise Echtzeit-Analysen."[@Weiss2015].

### Funktionsweise

Der Grund für die genannten Vorteile in Performance sind auf die interne Funktionsweise zurückzuführen. Um zu verstehen, wie diese die Performance erhöht, werden die internen Vorgänge näher beleuchted.

Das Grundlegende Konzept des Spark Frameworks sind sogennante **Resilient Distributed Datasets (RDD)**. Im Folgenden wird das Konzept eines RDDs vorgestellt und die Datenverarbeitung innerhalb mit RDDs erläutert. 

#### Resilient Distributed Dataset (RDD) {#sec:cpt-rdd}

Ein RDD ist eine strukturierte Sammlung an Daten, welche oft als Informationstabelle dargestellt wird. Jedoch kann jede Art von Datentyp in der Struktur gespeichert werden. Spark speichert eine einzelne RDD auf mehreren (physisch getrennten) Partitionen.

RDDs helfen dabei Daten zu verwalten und durch Optimierung von Verarbeitungsschritten die Effizienz zu erhöhen. 

Sie sind ausfalltolerant, da RDDs in der Lage sind den Zustand der Daten erneut aus den Ursprugsdaten zu berechenen, sollten diese verloren gehen. Informatioen dazu werden in einem **Directed Acyclic Graph (DAG)** gespeichert (siehe nächstes Kapitel).

RDDs sind unveränderbar. Transformationen (Operationen auf den Daten des RDDs) erzeugen ein neues RDD, während das ursprüngliche bestehen bleibt.

Die Interaktion und Datenverarbeitung mit RDDs ist eine der wichtigsten Eigenschaften der Datenstruktur. Ein RDD kann wie ein Objekt genutzt werden. Es bieten high-level Operationen um zur Entwicklungszeit Bedienbarkeit und Flexibilität zu garantieren und während der Laufzeit bestmögliche Performance zu zeigen[@Zahariab]. Wie die Datenverarbeitung mit RDDs intern funktioniert wird im nächsten Kapitel erklärt.

#### Datenverarbeitung mit RDDs

Die Datenverarbeitung mit RDDs weist Unterschiede zu dem MapReduce Algorithmus: Anstelle einer *Map* und *Reduce* Operation, unterstützt Spark *Transformation* und *Action*:

- **Transformation**

  Durch eine Transofmartion, wird ein neues RDD aus einem bestehenden RDD erzeugt. Dabei werden jedoch noch keine Daten ausgewertet oder Ergebnisse erzeugt. Spark nutzt *Lazy Evaluation*. Dabei erfolgt Datenauswertung und Anwendung von Operationen erst, wenn das Ergebnis dieser benötigt wird. Transformationen umfassen beispielsweiße:

  *map, filter, flatMap, groupByKey, reduceByKey, aggregateByKey, pipe, coalesce*

- **Action**

  Wird eine Action ausgeführt, wird ein Ergebniss ausgegeben. Dies bedeuted, dass nötige Operationen der vorherigen Transformationen durchgeführt werden und die Ergebnisse der verteilten Berechnung aggregiert an den Aufrufer der Action zurückgegeben werden. Aktionen umfassen beispielsweise:

  *reduce, collect, count, first, take, countByKey, foreach*

Beide Operationen werden im Arbeitspeicher ausgeführt, ohne Daten dabei zurück in den persistenten Speicher zu schreiben.

Die folgenden Abbildung zeigt, wie Datenverarbeitung durch Transformationen und Aktionen mit RDDs funktionert.

![Verhalten eines RDD bei Transformationen und Aktionen[@Ghadiyaram]][DAG]

Zuerst wird ein RDD mit Daten auf dem Cluster erzeugt. *Transformationen* erzeugen neue RDDs. Es können beliebig viele Transformationen durchgeführt werden. Dies unterscheidet Spark von dem Hadoop MapReduce Algorithmus, welcher nur eine *Map* Funktion zulässt bevor die Daten wieder in den Specher geschrieben werden. Die Transformationen werden nicht direkt ausgeführt, sondern in einem **Directed Acyclic Graph (DAG)** gespeichert (hier genannt Lineage, engl. Abstammung). Eine *Aktion* löst die Berechnung aller nötigen Transformationen auf den Daten aus und aggregiert diese um eine Endergebnis zu erzeugen. Das Prinzip, Transformationen erst dann auszuführen wenn diese Nötig sind, wird als *Lazy Evaluation* bezeichnet. Es reduzierte Komplexität, bietet Möglichkeiten zur Optimierung und performanten Anordnung der Operationen und spart Berechnungen, die nicht für das Endergebniss relevant sind um Geschwindigkeit zu erhöhen [@Zahariab].

### Anwendung und Libraries {#sec:cpt-spark-libs}

Spark bietet high-level APIs in Scala, Java, und R für die folgenden Libraries, welche auf dem Spark Core aufbauen. Die Libraries nutzten die verteilte Berechnung, welche vom Spark Core zur Verfügung gestellt wird.

![Spark Bibliotheken][sparklibs]



###### Spark SQL

SQL Queries können auf strukturierten Daten in RDDs angewandt werden. Dazu wird die Struktur eines sogenannten *DataFrames* genutzt, welcher auf den RDDs basiert. Dies ermöglicht die flexible Kombination aus SQL Queries und Standard RDD Operationen, wie zum Beispiel Transformationen.

Daten können von einer Vielzahl an Quellen integriert werden und Daten verschiedener Quellen sogar in einer Abfrage kombiniert werden [@Xin].

```python
results = context.sql("SELECT * FROM people")
names = results.map(lambda p: p.name)
```
###### Spark Streaming

Kontinuierliche Datenströme aus verschiedenen Quellen werden verarbeitet (engl.: stream processing) [@Zaharia].

```python
TwitterUtils.createStream(...)
    .filter(_.getText.contains("Spark"))
    .countByWindow(Seconds(5))
```
###### MLlib

Komplexe Machine Learning Modelle werden in einem verteilten Cluster erzeugt und trainiert [@Meng2016].

```python
data = spark.read.format("libsvm").load("hdfs://...")
model = KMeans(k=10).fit(data)
```
###### GraphX

Graphbasierte Daten werden auf Basis von RDDs effizient verarbeitet [@Xina].

```python
graph = Graph(vertices, edges)
messages = spark.textFile("hdfs://...")
graph2 = graph.joinVertices(messages) {
  (id, vertex, msg) => ...
}
```
Neben den genannten offiziellen Libraries, besitzt Apache Spark auch eine große Anzahl an sogenannten *Third Party Projects*, welche neben neuen Funktionalitäten auch Kompatibilität und Konnektivität, beispielsweiße zu externen Datenquellen, bieten.

### Third Party Projects: Anbindung externer Datenquellen

Das Apache Spark Framework läuft unter einer freien Apache Lizenz. Aufgrund dessen existieren viele Projekte der Community oder externer Mitwirkender. Neben den offiziellen Libraries erweitern diese Projekte das Framework um zusätzliche Funktionalitäten. 

Neben komplett neuen Funktionalitäten, stellen Projekte auch Kompatibilität und Konnektivität zu externen Systemen zur Verfügung. Die Anbindung von verschiedensten Datenquellen wird so ermöglicht. 

Das Projekt  "**Spark Cassandra Connector**" wird von dem Softwareanbieter DataStax entwickelt, dem Hauptmitwirkendem des Apache Cassandra Projektes. Das Projekt ermöglicht eine performante Anbindung einer Apache Cassandra Datenbank an das Apache Spark Framework und nutzt dabei Synergien zwischen den beiden Systemen [@ApacheSpark]. 

Im nächsten Kapitel wird näher erläutert, wie diese Verbindung geschaffen wird und Synergien lohnenswert genutzt werden.


