## Vergleich zu anderen NoSQL-Datenbanken

Neben Column-Family-Datenbanken gibt es noch andere NoSQL-Datenbanktypen. Diese sind zum Beispiel Key-Value, dokumentenorientierte oder Graph-Datenbanken. Die bekanntesten Datenbanksysteme sind Redis (Key-Value-Datenbanken), MongoDB (dokumentenorientierte Datenbank) und Neo4J (Graph-Datenbank). Neben den eben genannten NoSQL-Datenbanktypen gibt es noch weitere Typen, die allerdings oftmals nicht sehr verbreitet sind, da sie nur für Einzelfälle genutzt werden. Dazu zählen zum Beispiel XML, Gitter oder Objekt-Datenbanken.[@Fasel2016;@Muller2014]

### Key-Value-Datenbanken

Key-Value-Datenbanken speichern Schlüssel-Wert-Paare und sind meistens besonders schnell, weil sie diese Paare im Arbeitsspeicher halten und somit eine schnelle Antwortzeit besitzen. Jedoch gibt es auch Key-Value-Datenbanken, die die Paare auf eine Festplatte schreiben und von dort auch wieder lesen.[@Fasel2016;@Meier2016]

### Dokumentenorientierte Datenbanken

Dokumentenorientierte Datenbanken speichern Datensätze als sogenannte Dokumente ab. Die Dokumente beinhalten die Informationen als Key-Value-Paare. Sie haben keine Verbindung zu Dateien auf einem Computer, sondern sind lediglich zum speichern von Informationen zu einem bestimmten Datensatz gedacht. Diese Dokumente werden in Kollektionen abgespeichert, wodurch eine bessere Unterteilung erreicht wird.[@Fasel2016;@Edward2015;@Harrison2015;@Meier2016]

### Graph-Datenbanken

Graph-Datenbanken bestehen im Gegensatz zu dokumentenorientierten Datenbanken nicht aus Dokumenten, sondern aus Knoten und Kanten. Knoten bilden die Datensätze ab. Hier werden die Daten, genau wie in dokumentenorientierten Datenbanken, als Key-Value-Paare gespeichert. Zwischen den einzelnen Knoten liegen Kanten, die die Knoten verbinden. Die Kanten können ebenfalls Key-Value-Paare als Informationen besitzen, sind jedoch nicht als Datenspeicher gedacht. Sie sind dafür gedacht, die Relation zwischen zwei Knoten darzustellen.[@Fasel2016;@Jordan2014;@Meier2016]

### Unterschiede

Der Unterschied zwischen den NoSQL-Datenbanktypen liegt im Verwendungszweck. Während Cassandra, also Column-Family-Datenbanken oftmals bei stark veränderlichen Daten genutzt werden, werden Key-Value-Datenbanken bei einfachen Daten, wie zum Beispiel Anwendungsvariablen, eingesetzt. Außerdem können die Daten in Key-Value-Datenbanken keine Verbindung untereinander haben, was eine relationale Logik außerhalb der Datenbank voraussetzt. Dokumentenorientierte Datenbanken sind für Daten gedacht, die kein eindeutiges Schema besitzen, wie zum Beispiel ein Adressbuch. Sie sind schemafrei, was bedeutet, dass nicht jedes Dokument demselben Schema folgen muss. Hier sind außerdem einfache Relationen zwischen Dokumenten möglich. Graph-Datenbanken sind ebenfalls für Daten gedacht, die keinem Schema unterliegen. Jedoch sind Graph-Datenbanken in erster Linie für Daten gedacht, die stark untereinander zusammenhängen. Dies wäre zum Beispiel bei einer Organisationsstruktur eines Unternehmens der Fall.[@Fasel2016;@Meier2016]

Jeder NoSQL-Datenbanktyp hat seinen speziellen Anwendungsfall, in dem er am besten eingesetzt werden kann und dort einen Vorteil gegenüber anderen NoSQL-Datenbanktypen besitzt.  
