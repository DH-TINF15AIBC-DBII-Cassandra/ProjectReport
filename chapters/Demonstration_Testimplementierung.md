<!-- 1. Quellcode schreiben(RDDLoad)/Ausbauen -->

<!-- 2. Quellen -->

<!-- 3. Umstrukturieren -->

<!-- 4. Kommentare einbauen -->

# Beispielimplementierung für das Zusammenspiel von Spark und Cassandra

## Einrichtung der Spark Umgebung

Spark ist in Scala geschrieben. Scala ist eine funktionale Skript Sprache für die Java VM. Das heißt das der entsprechende Scala Source Code in Java übersetzt wird. Spark hat außerdem direkte Schnittstellen zu R, Python und Java. Für dieses Anwendungsbeispiel Scala wird genutzt, da dieses die umfangreichste API für Spark besitzt. Dafür muss zuerst die Scala Sprache installiert werden. Dafür muss zunächst der Scala Compiler installiert werden. 
Der aktuelle Installer für Windows ist bei der offiziellen Scala Webseite herunterzuladen [Link](https://www.scala-lang.org/download/). Für dieses Beispiel wurde Scala [2.12.4](https://downloads.lightbend.com/scala/2.12.4/scala-2.12.4.msi) installiert. 
Zum Laden und Installieren der Dependencies wird zudem ein Build Tool benötigt. Für Scala/Java Projekte sind Maven oder das Scala Build Tool (SBT) die meist verwendenden Tools. Dieses Beispiel nutzt SBT, weil es einfacher zu nutzen ist als Maven. Im Anhang findet sich jedoch auch eine beispielhafte Integration für Maven.
SBT für Windows kann über die offizielle [SBT Webseite](https://www.scala-sbt.org/download.html) heruntergeladen und installiert werden. Dieser Bericht basiert auf SBT Version [1.1.1](https://github.com/sbt/sbt/releases/download/v1.1.1/sbt-1.1.1.msi).

Das Projekt wird in der Entwicklungsumgebung [IntelliJ IDEA](https://www.jetbrains.com/idea/) entwickelt. Es existieren jedoch auch andere IDEs die Scala unterstützen wie z.B. Eclipse.

Unter Linux (und macOS) läuft Apache Spark *out of the box*, wenn es vom Build Tool geladen wird. Unter Windows sind extra Schritte für die Einrichtung notwendig, welche im Folgenden beschrieben werden. Dafür müssen Spark und Hadoop zuerst von der offiziellen Apache Seite herunter geladen werden ([Link](https://spark.apache.org/downloads.html)). Anschließend müssen die Archive in ein Verzeichnis ohne Leerstellen entpackt werden.

Des Weiteren werden die Winutils für die entsprechende Hadoop Version benötigt. Diese sind GitHub Repo vom Entwickler Steve Loughran zu finden ([Link](https://github.com/steveloughran/winutils/releases)). Die entsprechenden Executables können in das `bin` Verzeichnis von Spark verschoben werden oder in einen eigenen Hadoop-Ordner ohne Leerzeichen im Pfad. Zudem müssen die verschiedenen System Umgebungsvariablen für die Hauptverzeichnisse  gesetzt sein. Für die Programmiersprachen: `JAVA_HOME`, `SCALA_HOME` - für die Frameworks: `SPARK_HOME`, `HADOOP_HOME` und für die Build Tools entweder `MAVEN_HOME` oder `SBT_HOME`. Um Java VM Fehler wie Out of Memory zu vermeiden wird auf Bedarf die `_JAVA_OPTION` Variable mit Optionen versehen. Das Testsystem dieser Anwendung ist mit folgenden Variablen definiert (Windows CMD Syntax):

```sh
setx JAVA_HOME "C:\Program Files\Java\jdk1.8.0_131" /M
setx SCALA_HOME "C:\Program Files (x86)\scala" /M
setx SPARK_HOME "C:\Programs\spark-2.3.0-bin-hadoop2.7" /M
setx HADOOP_HOME "C:\Programs\hadoop-2.8.1" /M
setx SBT_HOME "C:\Program Files (x86)\sbt" /M
```
: Setzen der System-Variablen unter Windows {#lst:env-spark}

Unter Linux kann dafür `export JAVA_HOME=...` genutzt werden. Anschließend müssen noch die Berechtigungen für den temporären Ordner von Apache Hive gesetzt werden. Sollte der lokale Derby-based meta-store **metastore_db** Ordner existieren, sollte dieser noch gelöscht werden.

```sh
winutils.exe chmod 777 \tmp\hive
rm -rf C:\Users\<User_Name>\metastore_db
```
: Berechtigungskonfiguration von Apache Hive unter Windows {#lst:env-hive}

War die Einrichtung erfolgreich, sollte die `spark-shell` ohne Fehler starten. 

```
C:\Users\PaulBauriegel>spark-shell
Setting default log level to "WARN".
To adjust logging level use sc.setLogLevel(newLevel). For SparkR, use setLogLevel(newLevel).
Spark context Web UI available at http://DESKTOP-83IMOU6:4040
Spark context available as 'sc' (master = local[*], app id = local-1521465647738).
Spark session available as 'spark'.
Welcome to
      ____              __
     / __/__  ___ _____/ /__
    _\ \/ _ \/ _ `/ __/  '_/
   /___/ .__/\_,_/_/ /_/\_\   version 2.3.0
      /_/

Using Scala version 2.11.8 (Java HotSpot(TM) 64-Bit Server VM, Java 1.8.0_131)
Type in expressions to have them evaluated.
Type :help for more information.

scala>
```
: Erfolgreicher Start der Spark-Shell {#lst:env-shell}

<!-- Erwähne Fehler beim beenden -->

## Einrichtung der Cassandra Datenbank

Die Cassandra Binaries können über zwei Wege installiert werden. Direkt von Apache mit einer manuellen Installation oder über den Installer von DataStax, welcher die Datenbank automatisch installiert.

Egal welcher Weg gewählt wird sind zwei Voraussetzungen zu beachten, damit die Startskripte unter Windows funktionieren.

1. Cassandra muss in einen Ordner ohne Leerzeichen installiert werden
2. Die `CASSANDRA_HOME` Umgebungs-Variable darf keinen Slash am Ende besitzen, 
   z.B. `C:\Programs\DataStax-DDC\apache-cassandra`

Die Testumgebung dieses Projektes nutzt den Installer DataStax Distribution of Apache Cassandra™ (DDC) in Version [3.9.0](http://downloads.datastax.com/datastax-ddc/datastax-ddc-64bit-3.9.0.msi), Der Installer gestaltet die Einrichtung und Verwaltung von Cassandra einfacher anhand von zwei Features [@Schumacher2012]. Es wird automatisch einen Cassandra Service (`DataStax_DDC_Server`) eingerichtet. Der reine Apache Build kann jedoch auch als Service Installiert werden. Dafür müssen die Commons Daemon Service Runner Binaries  (`prunsrv.exe`) im `bin\daemon` directory gespeichert. Anschließend wird mittels `cassandra.bat install` cassandra als Windows Service (`cassandra`) installiert [@Savas2013].

![Dev Center Test-Query](images/DevCenter_Test-Queries.PNG){#fig:devcenter}

Der DataStax Installer bringt zudem die Verwaltungsanwendung *DataStax DevCenter* mit, siehe Abbildung [-@fig:devcenter]. Die auf dem Eclipse Framework basierte Anwendung ermöglicht es auf verschiedenen Cassandra Instanzen Abfragen zu testen. Die Cassandra Instanz der Test Applikation läuft standardmäßig auf dem localhost unseres Testsystems am Port 9042 ohne Authentication. 

## Entwicklungsworkflow der Beispielanwendung

Das folgende Kapitel erklärt eine Beispielanwendung, welche das Zusammenspiel zwischen Spark und Cassandra veranschaulichen soll. Dabei wird auf die Einrichtung einer Scala Applikation mit Apache Spark und dem Cassandra Connector gezeigt. Anschließend wird auf verschiedene Anwendungsfälle eingegangen, welche die Anwendung abdeckt. 

### Minimalistischte Aufbau eines Spark-Cassandra Beispiels {#sec:cpt-min-bsp}

Der minimalistischte Aufbau einer solchen Anwendung besitzt eine `build.stb` Datei für das Scala Build Tool und eine einfache `Application.scala` für den eigentlichen Quellcode welcher vom Scala Compiler ausgeführt wird. Um die Anwendung ohne Entwicklungsumgebung zu starten reicht ein einfacher Aufruf von: `sbt run`.

#### Aufbau der Build Datei

Die Anwendung benötigt mehrere Abhängigkeiten aus dem Spark Namespace sowie den spark-cassandra-connector. Das folgende Quellcode-Listing zeigt diese Dependencies und deren Versionen. Auch wenn Scala aktuell in Version 2.12 existiert können wir diese nicht nutzen. Die Spark existiert aktuell nur für Scala 2.11. Nutzt man die Bibliotheken von 2.11 zusammen mit den Connector führt jedoch noch zu einem Fehler beim der SBT Build. Aus diesem Grund verwendet die Anwendung noch Scala 2.10 mit den neusten Versionen der Bibliotheken dafür.

```scala
name := "DHBW-DBII-SparkCassandra"

version := "0.1"

scalaVersion := "2.10.4"
scalacOptions += "-deprecation"

libraryDependencies += "org.apache.spark" %% "spark-core" % "2.2.1"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.2.1"
libraryDependencies += "org.apache.spark" %% "spark-catalyst" % "2.2.1"

libraryDependencies += "com.datastax.spark" %% "spark-cassandra-connector" % "2.0.7"
```
: Minimale Konfiguration der build.sbt {#lst:min-example-sbt}

#### Einfaches Beispiel Schema für Cassandra

<!-- More Links to previous Chapters -->

Um den Spark Cassandra Connector aus der Scala Anwendung zu testen wird eine laufende Cassandra Instanz mit einem einfachen Beispiel Schema benötigt. Das folgende CQL Code Schnipsel erstellt einen einfach replizierten Keyspace mit einer einfachen Tabelle namens *posts* [@Schumacher2012]. 

```CQL
CREATE keyspace if not exists social 
	with replication={'class':'SimpleStrategy', 'replication_factor':1};

USE social; 

CREATE TABLE posts (
	user_id int,
	username varchar,
	creation timeuuid,
	content varchar,
	PRIMARY KEY ((username), creation)
);

INSERT INTO posts (user_id, username, creation, content)
	values (1, 'Fred', now(), 'Hi there!');
INSERT INTO posts (user_id, username, creation, content)
	values (2, 'Alice', now(), 'Apache Spark rules.');
INSERT INTO posts (user_id, username, creation, content)
	values (3, 'Bob', now(), 'Long live Apache Cassandra!');
INSERT INTO posts (post_id, username, creation, content)
	values (4, 'Joshua', now(), 'Burn down Oracle!!!');
	...
```
: Erstellung des CQL Schemas (Social Schema) {#lst:min-example-cql}

Anhand dieser Daten lässt sich zeigen, wie sich Abfragen mit der Komplexität von SQL auf NoSQL Datenbank wie Cassandra durchführen lassen. Dies ist unter anderem durch die Nutzung von Spark möglich. Dafür wird im Folgenden die grundlegende Einrichtung einer solchen Anwendung beschrieben. Eine Spark Anwendung wird durch das Erstellen einer Spark-Session grundsätzlich eingerichtet. <!-- umschreiben? -->Die Anwendung arbeitet anschließend in dem Spark-Context um auf die Cassandra Daten zuzugreifen und die API von Spark für die Tabellen zu nutzen. Die Session Factory hat drei wichtige Konfigurationsparameter. Das `spark.executor.memory` wird auf auf 1G beschränkt, sodass unsere Testumgebung nicht mehr Memory allokiert als ihm zu Verfügung steht. Die Anwendung wird mittels `master("local[*]")` im Local-Mode gestartet. Um Spark verteilt auf einem Cluster auszuführen müsste diese Configuration geändert werden. Der Spark-Cassandra Connector benötigt zudem die Configuration der IP-Adresse  (`spark.cassandra.connection.host`) um die Cassandra Instanz anzusprechen. [@Ess2015]

\pagebreak
<!-- Local vs Cluster mode -->

```scala
val sparkSession = SparkSession.builder
    .appName("DataTransformation")
    .config("spark.executor.memory", "1g")
    .config("spark.cassandra.connection.host", "localhost")
    .master("local[*]")
    .getOrCreate
  val sc = sparkSession.sparkContext
  val table = sc.cassandraTable("social", "posts")
  println(table.first)
```
: Enfaches Auslesen einer Cassandra-Tabelle in Spark {#lst:min-example-spark}

Nach der Configuration und Erstellung der Spark Session erlaubt der Cassandra Connector es mit *cassandraTable* auf eine Tabelle zuzugreifen. In dem obigen Beispielcode greift das Script auf die posts-Tabelle im Keyspace social zu. Ein solcher Aufruf gibt ein CassandraRDD Object zurück. Dieses Object ist die Implementierung der im Kapitel [-@sec:cpt-rdd] besprochenen RDDs durch den Cassandra Connector.

#### Erweiterung der Cassandra Abfragen um die Möglichkeiten Sparks

Mit Zugriff auf unser Cassandra Datenbank können wir nun verschiedene Abfragen realisieren, welche CQL nicht ermöglicht. Dafür nutzen wir die Beispiele für unmögliche Cassandra Abfragen aus Kapitel [-@sec:cpt-cql-schwach]. Die folgende Ausgabe der CQL-Shell zeigt die Limitierungen durch CQL in Form von Abfragen, welche nicht verarbeitet werden konnten oder syntaktisch unmöglich sind.

```sql
cqlsh:social> SELECT * FROM posts
          ...     WHERE username='Fred'
          ...     ORDER BY content
          ... ;
InvalidRequest: Error from server: code=2200 [Invalid query] message="Order by is currently only supported on the clustered columns of the PRIMARY KEY, got content"
cqlsh:social> SELECT * FROM posts
          ...     WHERE content LIKE 'Oracle%'
          ... ;
InvalidRequest: Error from server: code=2200 [Invalid query] message="LIKE restriction is only supported on properly indexed columns. content LIKE '%Oracle%' is not valid."
cqlsh:social> SELECT * FROM posts
          ...     ORDER BY creation
          ... ;
InvalidRequest: Error from server: code=2200 [Invalid query] message="ORDER BY is only supported when the partition key is restricted by an EQ or an IN."
cqlsh:social> SELECT * FROM posts
          ...     JOIN ratings ON posts.post_id = ratings.post_id
          ... ;
SyntaxException: line 2:4 no viable alternative at input 'JOIN' (SELECT * FROM [posts]    JOIN...)
```
: Beispiel für unmögliche CQL Abfragen {#lst:impossible-cql}

<!-- alle sechs Analytic Music Beispiele der GIT Test Anwendung noch einmal durchgehen -->

Anhand der vier Beispiele werden die verschiedenen Abstraktionsstufen der Spark API gezeigt, sowie deren Funktionalitäten. 

##### Tabellenoperationen über Sparks RDD API

Das Startbeispiel des vorherigen vorherigen Kapitels greift mit der unterster API Ebene, direkt über die RDDs, auf die Cassandra Tabelle zu. Folgendes Codebeispiel zeigt die Lösung für das erste CQL-Beispiel [-@lst:impossible-cql]. 
Die auf Fred gefilterten Daten werden dabei auf eine gemeinsame Partition gebündelt. Würde die Neupartition nicht durchgeführt, werden alle Partitionen separat sortiert. Somit existieren nur sortierte Teillisten, welche die print-Ausgabe nacheinander ausgibt. Ist die Reihenfolge der Ausgabe nicht notwendig, müssen die Daten auch nicht auf eine partition gepresst werden, wie das Filter-Beispiel zeigt.<!-- x=>x... kurz zu _ -->

```scala
table.where("username=?","Fred").repartition(1).sortBy(row => row.getString("content"))
     .foreach(println)
table.filter(_.getString("content").contains("Oracle")).foreach(println)
```
: Filtern und Sortieren mit der Spark RDD API {#lst:rdd-filter}

Das obige Beispiel zeigt wie die Daten beliebt gefiltert und sortiert werden können. Auf RDD Ebene erlaubt Spark zudem einfaches Kombinieren von Tabellen über Inner (*joinWithCassandraTable*) und LeftOuter-Joins (*leftJoinWithCassandraTable*):

```scala
sc.cassandraTable("social", "ratings").joinWithCassandraTable("social","posts")
  .foreach(println)
```
: Joins mit der Spark RDD API {#lst:rdd-join}

##### Tabellenoperationen über Sparks DataSet API

<!-- DataSet Teil von Catalyst oder SQL -->Seit 2015 existiert mit den DataFrames eine high-level API um mit strukturierten Daten, wie Cassandra Tabellen zu arbeiten. 2016 wurde zudem die DataSet API eingeführt, welche strengere Typisierung erlaubt. Seit Version 2.0 sind beide APIs unter der gemeinsamen DataSet API zusammengefasst. Das folgende Kapitel zeigt die Funktionalitäten der API anhand unserer CQL-Beispiele. Um eine Cassandra-Tabelle in ein Spark DataSet zu laden, wird folgender Syntax genutzt: 

````scala
val df: DataFrame = sparkSession.read.cassandraFormat("posts", "social").load
````
: Lesen ein Cassandra-Tabelle in ein Spark Dataframe {#lst:dataset-access}

Die folgenden beiden DataSet-Beispiele zeigen eine äquivalente Implementierung zu den Codebeispielen aus dem vorherigen Kapitel. Man kann erkennen, dass die Funktionen der Schnittstellen deutlich einfacher gehalten sind. Und zudem einen deutlich größeren Funktionsumfang liefern. <!--Datatype definition -->

```scala
df.filter(df("username")==="Fred").orderBy("content").show
df.filter(df("content").like("%Oracle%")).show
```
: Filtern und Sortieren mit einem Spark Dataframe {#lst:dataset-filter}

So bietet der Filter unter anderem die Möglichkeit, nicht nur nach Fred sondern auf eine Liste von Nutzern zu filtern. Mit dem Filter `df("username").isin(list:List[String])` ist eine beliebige Liste möglich.
Auch beim joinen von Tabellen gibt es mehr Möglichkeiten wie das folgende Beispiel zeigt. So ist eine komplexere Join Bedingung möglich und auch Cross-Joins sind implementiert.

```scala
val join: DataFrame = sparkSession.read.cassandraFormat("ratings", "social").load
df.join(join, joinExprs = df("post_id")===join("post_id"), joinType = "left_outer").show
```
: Joins von Spark Dataframes {#lst:dataset-join}

##### SQL Operationen mit dem SQL Context

````scala
sparkSession.sqlContext.sql(
"""
|SELECT * FROM posts
| WHERE username='Fred'
| ORDER BY content
""".stripMargin).show
sparkSession.sqlContext.sql(
"""
SELECT * FROM posts
JOIN ratings ON posts.post_id = ratings.post_id
""").show
````
: Ausführung von SQL über den Spark SQL Context {#lst:sqlcontext}


Als weiter Abstraktionsmöglichkeit kann Spark auch ähnlich wie normale Datenbank genutzt werden. Dafür gibt es SparkSQL und den SQLContext. Der SparkSQL Context  ermöglicht beinahe alle Arten von SQL Abfragen. Für einige spezielle SQL Abfragen welche nicht funktionieren, kann auch Apache Hive als alternativer SQLContext initialisiert werden. Apache Hive ist ein vollwertiges Datawarehouse, welche den SQL Syntax über MapReduce umsetzt. Eine präzisere Ausführung zu Hive findet sich im folgenden Kapitel [-@sec:cpt-kompl-sql].<!-- Quelle--> Das folgende Beispiel zeigt zwei solcher Beispiele welche in CQL nicht möglich wären:

<!-- Sicher das hier kein Hive kommen soll? -->

### Importieren und Exportieren von Daten in Cassandra {#sec:cpt-import}

Um für die folgenden Anwendungsbeispiele mit größeren und komplexeren Datensätzen zuarbeiten, erklärt dieses Kapitel wie bestehende Datensätze oder Datenbanken in Cassandra importiert werden. Dies ist vor allem für den Einsatz in bestehenden Infrastrukturen von Cassandra wichtig, da es als Ersatz für existierende Datenbanken genutzt wird oder bestehende Datensätze integriert. Ein Export einer CQL-Abfrage kann ebenfalls für eine Anwendung relevant werden und ist deshalb Teil dieses Kapitels. <!-- Performance Comparision--> <!-- Tabellenschema muss feststehen-->

#### Einfacher Import und Export über die CQLSH

Es gibt mehrere Wege Daten in Cassandra zu laden. Der einfachste Weg mit geringster Komplexität ist der Import-Befehl `COPY FROM` bzw der Export-Befehl `COPY TO` über CQL. Zusammen mit diesem Befehl sollten zudem auch Parameter wie Header oder Format der Zeitspalte angegeben werden, andern falls kann es zu ungültigen Queries kommen, weil cassandra die Daten nicht casten kann.

````cql
COPY posts(user_id, username, creation, content)
FROM 'posts.csv'
WITH DELIMITER=',' 
	AND HEADER=true 
    AND TIME_FORMAT='%Y-%m-%d %H:%M:%SZ;
````
: Import von Tabellen über die CQL-Shell {#lst:import-cqlsh}

Für kleine Datensätze der Weg über die Shell der beste. Je größer die Datensätze jedoch werden umso inperformater und fehleranfälliger ist diese Methode. Die CQL-Shell ermöglicht zudem keine Aufbereitung der Daten. <!-- https://www.datastax.com/dev/blog/simple-data-importing-and-exporting-with-cassandra -->

#### Schreiben und Lesen über Spark

Werden die Daten in den Spark-Context geladen ist eine Aufbereitung der Daten mit dem vollen Umfang der Spark API möglich. Dieses Kapitel erläutert kurz wie tabulare Daten und Datenbanken in Spark importiert werden und wie Spark Tabellen in Cassandra erstellen oder aktualisieren kann. Allen hier dargestellten Optionen ist gleich, dass sie ein existierendes Tabellenschema in Cassandra benötigen oder dieses selber erstellen müssen.
Wie bereits im Kapitel [-@sec:cpt-spark-libs] erwähnt besitzt Spark mehrere API Level mit unterschiedlicher Mächtigkeit. Textuelle Repräsentationen von Tabellen, wie z.B. CSV Dateien lassen sich relativ einfach mit der RDD API in eine Cassandra Tabelle überführen.

```scala
sc.textFile("path/to/posts.csv")
  .zipWithIndex()
  .filter {case (line, index) => index > 0}	//Skip Header
  .map{case (line,index) => {
    val lines = line.split(",") 		    //Delimiter, Cast Array elements?

  	(lines(0).toInt, lines(1), UUIDs.startOf(lines(2)), lines(3)) // Date format
  }}
  .saveToCassandra("social", "posts", SomeColumns("user_id", "username", "creation", "content"))
```
: Lesen und Importieren von Tabellen über Spark RDD {#lst:import-spark}

Genauso einfach ist es auch Cassandra-Tabellen in textuellen Format abzuspeichern.

```scala
sc.cassandraTable("social", "posts").saveAsTextFile("export/posts.csv")
```
: Exportieren von Cassandra Tabellen über Spark RDD {#lst:export-spark}

Die Limitierungen der RDD API liegt in diesem Anwendungsfall in der Summe der  unterstützen Formate, sowie in den Möglichkeiten die Daten aufzubereiten. Das folgende Beispiel zeigt die Übertragung einer CSV-Datei in eine Cassandra Tabelle. 

```scala
val df = sqlContext.read
  .option("header","true")
  .option("delimiter", ",")
  .option("charset",StandardCharsets.UTF_8.name())
  .option("nullValue", "null")
  .csv("path/to/posts.csv")
// Some Dataframe operations ...
df.write.format("org.apache.spark.sql.cassandra")
  .mode(SaveMode.Append)
  .options(Map(
    "table" -> "users",
    "keyspace" -> "social",
    "confirm.truncate"-> "true",
    "header" -> "true",
    "output.batch.grouping.key" -> "none",
    "cassandra.output.throughput_mb_per_sec" -> "1MB"
  )).save
```
: Schreiben von Spark Dataframes in Cassandra {#lst:import-dataframe}

Nach dem Lesen der Daten können mittels Spark aufbereitet werden. Damit ist es möglich fehlerhafte oder inkonsistente Daten aufzubereiten oder rauszufiltern. Beim Schreiben der Daten in eine Cassandra Tabelle lassen sich Optionen zum Überschreiben der Daten in der Tabelle (SaveMode) sowie Optionen an den Cassandra Connector übergeben.

Der große Funktionsumfang der DataFrame API ermöglicht einfachere und besser automatisierbare Workflows für den Datenimport. Ein Beispiel dafür ist die Datenübertragung von einer relationalen Datenbank in Cassandra. Anstelle des Exportes aller Tabellen über die Herstellertools ist auch ein direkter Zugriff von Spark mittels der JDBC Schnittellen nutzbar. 

```scala
sqlContext.read.format("jdbc")
  .options(
      Map("url" -> "jdbc:sqlite:path/to/database.sqlite", 
          "dbtable" -> "Player"))
  .load
  .collect.foreach[Unit](row => ..)
```
: Lesen von Tabellen aus relationalen Datenbanken {#lst:import-rdbms}

#### Datenimport über den SSTableLoader

<!-- SSTABLE EXport -->Cassandra speichert alle Tabellen in dem internen Format der SSTables (siehe Kapitel [-@sec:cpt-save-cassandra]). Diese SSTables können ebenfalls für Im- und Exportprozesse genutzt werden. Dies vereinfacht den Export und Import zwischen verschiedenen Cassandrainstanzen. SSTables eigenen sich aber auch wenn ein Datensatz auf viele Cassandra Instanzen importiert werden muss, bzw. zu Testzwecken regelmäßig neu importiert wird. Für den Bulk-Load in solchen Anwendungsfällen stellt DataStax mit dem `CQLSSTableWriter` eine API bereit, welche es möglicht Dateien in diesem Format zu erstellen. [@Morishita2014]<!-- Nachteile aller Methoden -> SQL Schema muss definiert sein, für SSTable Loader casting of each element -->
Zunächst müssen im Script die `CREATE` und `INSERT` Statements für jede Tabelle im CQL Syntax hinterlegt werden. Diese Statements werden dem `CQLSSTableWriter` übergeben, zusammen mit weiteren optionalen Einstellungen wie BufferSize oder Partitioner.

```scala
val writer = CQLSSTableWriter.builder
  .inDirectory("path/table_folder") // set output directory
  .forTable(SCHEMA_PLAYER) 			// set target schema
  .using(INSERT_PLAYER) 		    // set CQL statement to put data
  .withBufferSizeInMB(10)
  .withPartitioner(new Murmur3Partitioner()) // set partitioner if needed
  .build
```
: Erstellen eines CQLSSTableWriters {#lst:sstable-object}

Dem TableWriter-Object werden dann iterativ die Zeilen eines DataFrames hinzugefügt.

```scala
df.foreach[Unit](row =>
    writer.addRow(
      new Integer(row.getInt(0)), new Integer(row.getInt(1)),
      row.getString(2), new Integer(row.getInt(3)), 
      row.getString(4), new Integer(row.getInt(5)), 
      new Integer(row.getInt(6)))
)
writer.close()
```
: Schreiben der Import-Zeilen mit dem CQLSSTableWriter {#lst:sstable-write-row}

Damit sind die SSTable Dateien erstellt worden und können mit Hilfe des sstableloader's in Cassandra geladen werden.

```bash
sstableloader path/table_folder
```
: Tabellenimport über den SSTableloader {#lst:sstable-loader}


<!-- #### Vergleich der einzelnen Ladeoptionen-->
<!-- Performance + Usability, verschiedene Datasets -->

### Komplexere Anwendungslogik mit Spark für Cassandra

Das vorausgehende Kapitel hat erläutert wie komplexere Datensets mit Spark und Cassandra genutzt werden. Dieses Kapitel gibt einen kurzen Einblick wie auch komplexere Anwendungslogik mit Spark für Cassandra umgesetzt werden kann. Hierfür wurden im Verlauf dieser Arbeit vier verschieden Implementierungen erstellt, welche ausgewählte Szenarien aus dem Kapitel [-@sec:cpt-cassanda] umsetzen. Schwerpunkt der Anwendungen primär die Präsentation der unterschiedlichen Spark Libraries im praktischen Kontext. Mit Ausnahme von GraphX und der Kafka Integration finden sich alle offiziellen Libraries in den Anwendungsbeispielen wieder.<!--was ist mit Hive--> Die folgenden vier Anwendungsfälle werden mit Hilfe dieser Skripte veranschaulicht:

**Komplexe Datenbanklogik für Cassandra** - Der Hauptteil dieser Anwendung wurde bereit im Kapitel [-@sec:cpt-min-bsp] vorgestellt. In diesem Kapitel wird auf zusätzliche Konzepte von Spark-SQL und HiveSQL eingegangen. <!-- ETL Process --> 
**Textanalyse für Foren oder  Social Media** - Dieses Beispiel ist relativ einfach gehalten und nutzt Cassandra nur als Datengrundlage. Nichtsdestotrotz ist der Anwendungsfall sehr wichtig, da er unter anderem sich zur Betrugserkennung auf Basis von Nachrichten oder Emails erweitern lässt.
**Machine Learning Pipeline** - Diese kurze Implementierung der Spark MLLib nutzt Cassandra ebenfalls nur als Datengrundlage. Machine Learning ist jedoch eines der wichtigsten Gründe Spark als verteiltes Framework zu nutzen. 
**Datenanreicherung mittels Live-Daten ** - Das letzte Beispiel demonstriert die Kombination von Spark und Cassandra um eine Datenbasis in schellen Intervallen mit Daten zu aktualisieren. Mit Hilfe der Spark Streaming Library wird das existierende Datenschema um Live Social Media Daten aus Twitter angereichert. Dabei werden Ereignisse auf Basis der Erwähnungen von Nutzern getriggert. <!-- sind das wirklich live Daten-->

Die wichtigsten logischen Bestandteile der Beispielanwendungen werden im Folgenden genauer dargestellt.

###### Spark Lazy Optimization

Startet man mit dem Schreiben einer Spark Anwendung und möchte diese im Anschluss testen, so sollte man im Hinterkopf behalten, dass Spark (Catalyst) Lazy Optimization betreibt. Das heißt vereinfacht gesagt, dass von dem Framework eine Optimierung des Laufzeitplan durchgeführt wird. Führt man also Berechnung in einem Script durch ohne die auch auszugeben, würde Spark diese weg optimieren.

```
scala> wordCount.toDebugString
res13: String =
(2) ShuffledRDD[21] at reduceByKey at <console>:24 []
 +-(2) MapPartitionsRDD[20] at map at <console>:24 []
    |  MapPartitionsRDD[19] at flatMap at <console>:24 []
    |  README.md MapPartitionsRDD[18] at textFile at <console>:24 []
    |  README.md HadoopRDD[17] at textFile at <console>:24 []
```
: Ausgabe des Laufzeitplanes {#lst:lazy-optimizer}

Dieser Laufzeitplan lässt sich zu Debugging-Zwecken für jedes RDD ausgeben, wie Listing [-@lst:lazy-optimizer] zeigt. Der Laufzeitplan lässt sich zudem über die SparkUI (http://masterhost:4040) abrufen und wird mit manchen Fehlermeldungen ausgegeben. Arbeitet man z.B. im SQL-Context über viele Tabellen und macht in einem der vielen Joins einen Fehler, so würde Spark den Laufzeitplan der Fehlermeldung mitgeben. <!-- Show Ontology Data Transformation -->

###### Komplexere SQL Logik in Spark {#sec:cpt-kompl-sql}
<!-- nach oben Verschieben? -->

Die Beispielimplementation für die Datenbanklogik nutzt noch zwei weitere Features der SparkSQL Library. Wie bereits angesprochen können ermöglicht die DataSet API eine Typisierung von Tabellen. Dabei wird eine `case class` für das Datenschema erstellt und auf das DataSet angewandt [@Pape2016]. Die Typisierung von DataSets kann zusätzliche Komplexität erzeugen, da z.B. nullable Spalten ein extra Handling benötigen (siehe Listing zu *Option[]*). In vielen Anwendungsfällen erleichtert die Typisierung jedoch die Arbeit mit dem DataSet. So ist es möglich neue Spalten programmatisch hinzuzufügen und der Code wird besser lesbar. Dies ist vor allem für komplexe Anwendungen hilfreich. <!-- Fehleranfälligkeit ist nicht gegeben -->  <!-- auf das Beispiel Social anwenden? -->

````scala
case class WeatherStation(id:String, name:String, countryCode:Option[String],
	stateCode:Option[String], callSign:Option[String], lat:Float, long:Float, 
	elvation:Float)
...
val germanWeatherStations = sc.cassandraTable[WeatherStation]("weather","weather_station")
      .filter( w => w.countryCode match {
          case Some(countryCode) => countryCode=="DL" || countryCode=="DD"
          case None => false
      })
      .map(station => (station.id, station.name)).collectAsMap()
...
Seq(WeatherStation(1, "Nordpol", "EN", "EN", "???", 23.23, 25.45, 0.0))
.toDF...
````
: Typisierung von DataSets über Case-Classes {#lst:typisation-dataset}

In einigen (seltenen) Anwendungsfällen kann es sein, dass die Syntax von SparkSQL nicht ausreicht. In diesen Fällen kann auch die mächtigere Syntax von Apache Hive genutzt werden. Apache Hive hat zusätzliche sogenannte *Windowing and Analytics Functions* welche zusätzliche Analyse in SQL ermöglichen. Wie in Kapitel [-@sec:cpt-spark-zus] zudem erläutert wurde können die MapReduce Alogrithmen von Hive in manchen Anwendungsszenarien Performancevorteile bringen. 

````scala
import org.apache.spark.sql.hive.HiveContext
...
val hiveContext = new org.apache.spark.sql.hive.HiveContext(sc)
import hiveContext._
hiveContext.sql("
|SELECT *, ROW_NUMBER() OVER () AS row_num
|FROM users".stripMargin)
````
: Abfragen im Hive-Context {#lst:hive-queries}

Verschiedene SQL Varianten können auch direkt in dem SQLContext genutzt werden indem ihr Format mittels `USING` spezifiziert wird. 
<!-- Source -->

```scala
sqlContext.sql("
|SELECT * FROM users
|USING org.apache.spark.sql.hive".stripMargin)
sqlContext.sql("
|SELECT * FROM users
|USING org.apache.spark.sql.cassandra".stripMargin)
```
: Abfragen in verschiedenen SQL-Varianten {#lst:sql-variants}

###### Einfache Textanalysen mittels Wörterzählen

Viele der im Kapitel [-@sec:cpt-cassanda] vorgestellten Anwendungsfälle für Cassandra, wie Fraud Detection oder Personalization, basieren auf Textanalysen. Zur Veranschaulichung einer Textanalyse mittels Spark enthält die Testanwendung ein einfaches Beispiel. Dieses Beispiel kann verschiedene Textstyle durch zählen der häufigsten Wörter unterscheiden. 

Das gezeigt Codebeispiel liest zunächst ein Textfile in ein RDD, wobei die Funktion *map_phase* alle Sonderzeichen u.ä. entfernt. Mittels `reduceByKey(_ + _)` wird die Anzahl aller Wörter gezählt, welche anschließend bei nach Vorkommen sortiert werden. Die Funktion detectText entscheidet anschließend auf Basis von Wortlisten in welche Kategorie der Text gehört. <!-- implementiere detectText --> <!-- schreibe über Russian Troll Tweets? --> <!-- many word rauslöschen --> <!-- schreibe auch über Ergebnisse/ Erkenntnisse -->

```scala
val counts = sc.textFile("data/text-analysis/books/Grundgesetz_Bundesrepublik_Deutschland.txt")
  .flatMap(line => map_phase(line))
  .map(word => (word, 1))
  .reduceByKey(_ + _)
val sorted = counts.sortBy(_._2, ascending = false).take(100)
sorted.zipWithIndex
  .foreach(x => println("%s: %s (%s)".format(x._2+1, x._1._1, x._1._2)))
detectText(sorted)
```
: Einfache textanalyse über Wörterzählen {#lst:textanalysis-wordcount}

Dieses Beispiel ist natürlich relativ einfach gehalten. Es existieren jedoch auch komplexere Anwendungen wie [diese](https://github.com/medale/spark-mail) [^spark-mail], welche die Anwendungsfälle wie Fraud Detection besser abdecken. Das aufgeführte Beispiel analysiert Emails aus dem Unternehmensskandal um Enron.

[^spark-mail]: https://github.com/medale/spark-mail

###### Ereignisse voraussagen über Sparks Machine Learning Pipeline 

Auf Basis der tabularen Fußball Daten, versucht das MLlib Beispiel den Ausgang eines Spiel vorherzusagen. Das Beispiel basiert auf einem Jupyter Notebook von David Sheehan. Ziel dieses Beispiels war es ein in Python geschrieben Machine Learning Modell in Spark zuportieren. Der folgende Beispielcode zeigt die Estimator-Pipeline von Apache Spark. Die textbasierten Eingabespalten HomeTeam, AwayTeam, home werden als Eingabevektoren der Feature Hashingfunktion welche *term frequency vectors* erstellt. Der Vektoren werden vom Term frequency-inverse document frequency (TF-IDF) Algorithms skaliert und anschließend in Test und Trainingsdaten aufgespalten. Damit kann nach Training des Modells dessen Erfolg überprüft werden.  Anschließend werden die Daten dem mit einer *generalized linear regression* auf Basis einer Poisson Verteilung trainiert. Die Vorhersagen können anschließend über die `glmModel.transform` Funktion getroffen werden. <!-- Bezug zu Tim -->

````scala
val wordsData = data.select($"label",array($"HomeTeam", $"AwayTeam",$"home").as("words"))

val hashingTF = new HashingTF()
.setInputCol("words").setOutputCol("rawFeatures").setNumFeatures(40)

val featurizedData = hashingTF.transform(wordsData)

val idf = new IDF().setInputCol("rawFeatures").setOutputCol("features")
val idfModel = idf.fit(featurizedData)

val rescaledData = idfModel.transform(featurizedData)

val Array(training, test) = rescaledData.randomSplit(Array(0.7, 0.3), seed = 12345)

val glm = new GeneralizedLinearRegression()
.setFamily("poisson")
.setLink("identity")
.setMaxIter(100)
.setRegParam(0.3)
val glmModel = glm.fit(training)

val transformed = glmModel.transform(test)
````
: Machine Learning Pipeline mit MLlib {#lst:mllib-mlpipeline}


###### Spark Streaming und Event Trigger

Neben Beispielen für MLlib und SparkSQL existiert auch eine Beispielanwendung für Spark Streaming, welche Cassandra extensiv nutzt. Als Streaming Grundlage  werden dabei die Twitter-Daten genutzt, welche über die Twitter Streaming Library eingebunden werden können. Das folgende Codebeispiel zeigt der Spark Stream erstellt wird. In jedem Batch Intervall werden die neuen Tweets von der *matchUser* Funktion auf  Erwähnung der Nutzer aus der Social Datenbank untersucht. Die Anzahl der Erwähnungen wird kontinuierlich in die *twitter_stream* Tabelle geschrieben.

```scala
val ssc = new StreamingContext(sc, batchDuration)
ssc.addStreamingListener(new MyStreamingListener(sparkSession))

val stream: ReceiverInputDStream[Status] = TwitterUtils
	.createStream(ssc, Some(authorization), Nil,StorageLevel.MEMORY_ONLY_SER_2)
	
stream.flatMap(_.getText.toLowerCase.split("""\s+"""))
    .filter(keywords.contains(_))
    .countByValueAndWindow(batchDuration, batchDuration)
    .transform((rdd, time) => rdd.map { 
    	case (keyword, count) => (matchUser(keyword), count, UUIDs.timeBased)})
    .saveToCassandra("social", "twitter_stream", 
    	SomeColumns("twitter_name", "count", "lastupdated"))
```
: Nutzung der Streaming Bibliothek für Tweets von Twitter {#lst:streaming-create}

Die Anwendung besitzt noch eine weitere Funktionalität. Mit dem in Listing [-@lst:streaming-listener] registrierten `StreamingListener` wird nach jedem Batchvorgang die Nutzertabelle um die aktuellen Twittererwähnungen aktualisiert. Dafür wurde die Klasse `MyStreamingListener` eingeführt welche durch überschreiben der `onBatchCompleted` regelmäßig die Benutzertabelle in der Cassandra-Datenbank überschreibt.

```scala
class MyStreamingListener(sparkSession: SparkSession) extends StreamingListener {

  override def onBatchCompleted(batchCompleted: StreamingListenerBatchCompleted) = {
  	...
    val ndf = df.join(n, df("twitter_name")===n("user_stream"),joinType = "left_outer")
      .drop("user_stream", "mentions_per_minute")
      .withColumn("new_count", when($"count".isNull, 0).otherwise($"count"))
      .withColumnRenamed("new_count", "mentions_per_minute").drop("count")

    ndf.write.format("org.apache.spark.sql.cassandra")...
  }
  ...
}
```
: Definition eine Streaming-Listeners \\& Automatischer Tabellenexport {#lst:streaming-listener}

###### Use Spark in Cluster-Mode

Spark auf zwei verschiedenen Arten ausgeführt werden. Für Testzwecke nutzt man in der Regel den Local-Mode in dem man dem die SparkSession mit `.setMaster(local[*])` konfiguriert. Entwickelt wurde Spark jedoch für dem Cluster-Mode. Für den Cluster-Mode wird ein JAR-Package ohne local Configuration gebaut. Diese kann anschließend mit einem Spark-Submit an das Cluster übergeben werden.

````bash
#!/usr/bin/env bash
spark-submit \
   --class com.ibm.datatransformation.Application \
   --executor-memory 20g \
   --driver-memory 25g \
   --total-executor-cores 400 \
   --conf spark.ui.port=30553 \
TestApplication-1.2.0.jar hdfs://some.host.url:8020/user/paulbauriegel/output/
````
: Spark Submit mit Parametern {#lst:spark-submit}

Eine Anwendung kann auch speziell für den Cluster-Mode optimiert werden. Ein Beispiel dafür findet sich in der Wetter-Query aus dem ersten Beispiel:

```scala
val stationsMap: Broadcast[Map[String, String]] = sc.broadcast(weatherStations)
```
: Broadcast Variablen für Lookup-Maps {#lst:broadcast-lookup}

Dabei wird eine Broadcast-Variable genutzt, welche die Lookup-Map (Stations-ID auf Name) an alle Worker verteilt. Durch die lokale Speicherung der Map auf jedem Worker ist die Verarbeitung schneller. Broadcast-Variablen funktionieren nur für klein Datentabellen, da sonst der Arbeitsspeicher dem Worker ausgehen würde.

## Gesamtüberblick für die Test Anwendung

Alle bisher beschriebenen Implementierungen und Codebeispiele liegen in einem gemeinsamen Repository, welches unter folgendem ([Link](https://gitlab.com/DH-TINF15AIBC-DBII-Cassandra/TestApplication)) bezogen werden kann. Die gesammelte Anwendung besteht aus mehreren Bestandteilen (siehe Listing [-@lst:overview-application]) über welche im Folgendem ein kurzer Überblick gegeben wird. <!-- keine Git spezifischen Sachen --><!-- build.sbt muss nicht nochmal neu erklärt werden oder? --> <!-- example data show? --> <!-- run with build.sbt?? -->

````
|   build.sbt
|---data
|   \---textdata
|   |   \---books
|   |   \---math.stackexchange.com
|   |   \---russian-troll-tweets
|   |   \---wikipedia
|   \---tabledata
|   |   \---football
|   |   \---weather
|   \---sqlschemas
|           social_schema.cql
|           test_spark.cql
|
\----src
    \----main
        \---resources
        |       log4j.properties
        \---scala
            \---de
                \---dhbwmannheim
                    \---sparkcassandra
                        \---dataload
                        |       SparkDataTransfer.scala
                        |       SSTableCreation.scala
                        |       importCQLSH.sh                   
                        \---usecase
                                DatabaseQueries.scala
                                DataStreaming.scala
                                MLLibPipeline.scala
                                TextAnalysis.scala
````
: Anwendungsüberblick {#lst:overview-application}

### Datensätze

Zum Testen der Anwendung wurden verschiedene Open-Datasets benutzt, welche sich im *data* Ordner befinden. Neben den Datensets finden sich in diesem Ordner auch unser Test-Schemata `social` sowie unsere Testabfragen. 

Zu den Datensätzen zählen verschiedene rein textuelle Daten unterschiedlicher Struktur und Größe. Twitter Tweets [^kraggle] von Nutzern welche als "Russische Trolle" eingestuft werden. Verschiedene rechtsfreie Schriften aus verschiedenen Epochen und von verschiedene Autoren. Die Bücher, Theaterstücke, Untertitel, religiöse Texte, Sachtexte oder Gesetze sind zudem in verschieden Sprachen in dem Verzeichnis. Ziel ist der unterschiedlichen Texte  ist mit einfachen Algorithmen die wesentlichen Unterschiede in diesen Texten zu erkennen [^bibliotheken] . Ein 8.7GB großen Messaging-Datenset des Forums Stackexchange [^stackexchange] findet sich zudem ebenfalls in diesem Ordner um Textanalysen auch einem umfangreichen Datenset testen zu können.

Ein weiterer Teil unserer Testdaten besteht aus Tabellen und Relationalen Datenbanken in CSV und SQLite Formaten. Die SQLite Datenbank ist eine Datenbank über historische Fußballergebnisse[^football], zusammengetragen aus drei Fußballdaten Foren. Dazu kommen noch einzelne aktuelle Fußball-Tabellendaten von football-data.co.uk. Auf Basis dieser Daten wurde das Machine Learning getestet. Um auch im Tabellenumfeld ein großes Testset zu haben, greifen wir zudem auf die hochauflösenden Wetterdaten des National Climatic Data Center (NCDC) zu [^wetter]. <!--TED Talk -->

Der dritte Ordner der Datensätze enthält alle CQL-Schemata und Abfragedateien welche als Beispiele benutzt wurden. Sowie die Schemas welche für den die In- und Export Skripte genutzt wurden. Zu den Abfragedateien gehören die Cassandra-Test für erfolgreiche und unmögliche Abfragen, sowie die cqlsh Import/Export Skripte. In dem Schemas findet sich das Social-Testschema, zwei verschiedene Fußball Schemata (für die SQLite Datenbank und die CSV Dateien), das Wetter-Schema, sowie ein Schema für die Textdaten.

[^kraggle]: https://www.kaggle.com/vikasg/russian-troll-tweets/data
[^bibliotheken]: http://www.digbib.org/	
[^stackexchange]: https://archive.org/details/stackexchange
[^football]: https://www.kaggle.com/hugomathien/soccer/data
[^wetter]: https://www1.ncdc.noaa.gov/pub/data/ghcn/daily/by_year/

### Beispiel-Implementierungen

Die ausführbaren Dateien der Testanwendung liegen im `src/main/scala/de/dhbwmannheim/sparkcassandra` Ordner. Unter `src/main/resources` finden sich zudem die Konfigurationsdateien für das Logging und für Apache Spark.

Die Anwendung besitzt zwei Pakete `dataload` und `usecase`. Unter `dataload` finden sich für jedes der drei in Kapitel [-@sec:cpt-import] erläuterten Import-Möglichkeiten ein Script.

**Einfacher Import und Export über CQLSH** - importCQLSH.sh
**Schreiben und Lesen über Spark** - SparkDataTransfer.scala
**Datenimport über den SSTableLoader** - SSTableCreation.scala

Im Usecase Ordner finden sich die vier in Kapitel [-@sec:cpt-cassanda] und [-@sec:cpt-use-spark] vorgestellten Beispielanwendungen.

**Komplexe Datenbanklogik für Cassandra** - DatabaseQueries.scala
**Textanalyse für Foren oder  Social Media** - TextAnalysis.scala
**Machine Learning Pipeline** - MLLibPipeline.scala
**Datenanreicherung mittels Live-Daten **- DataStreaming.scala

Für die Nutzung des Data Streaming's über die Twitter API muss jedoch zunächst eine Application in Twitter erstellt werden (`https://apps.twitter.com/app/new`). Dafür ist ein Twitter Account notwendig. Mit dem Erstellen der Anwendung bekommt der Nutzer eine *Consumer Key* und ein *Customer Secret*, welche unter `https://apps.twitter.com/app/<APPID>/keys` eingesehen erden können. In dieser Ansicht können auch der Access Token und das Access Token Secret generiert werden. Die müssen dann der Anwendung als Environment Parameter übergeben werden.

````
-Dtwitter4j.oauth.consumerKey="..." 
-Dtwitter4j.oauth.consumerSecret="..."
-Dtwitter4j.oauth.accessToken="..." 
-Dtwitter4j.oauth.accessTokenSecret="..."
````
: Umgebungsvariablen für das Twitter Streaming {#lst:example-env}