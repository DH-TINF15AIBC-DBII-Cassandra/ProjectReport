# Cassandra

Apache Cassandra zählt zu den populärsten NoSQL Datenbanken [@SolidITGmbH]. Sie ist eine spaltenorientierte und verteilte Datenbank, welche sich besonders für die Speicherung großer Datenmengen eignet. Zu ihren besonderen Stärken zählt die horizontale Skalierung, sowie die Ausfallsicherheit.

## Geschichte von Cassandra

Cassandra wurde von Avinash Lakshman und Prashant Malik bei Facebook entwickelt. Die erste Version von Cassandra wurde 2008 veröffentlicht. Anfang des Jahres 2009 wurde Cassandra von Facebook an den Incubator der Apache Software Foundation übergeben, um Cassandra unter deren Dach als Open-Source-Software zu veröffentlichen und weiterzuentwickeln. [@BigdataCassGrundlagen190318]

Nach der Öffnung von Cassandra als Open-Source-Projekt haben neben Facebook auch viele andere namhafte Unternehmen, wie zum Beispiel Twitter, IBM und Rackspace, zur Weiterentwicklung von Cassandra beigetragen. [@BigdataCassGrundlagen190318]

## CAP Theorem

Im Jahre 2000 wurde von Eric Brewer das CAP Theorem aufgestellt. Dieses Theorem besagt, dass die drei Eigenschaften von verteilten Datenhaltungssystemen, die Konsistenz (engl. *Consistency*), die Verfügbarkeit (engl. *Availability*) und die Ausfalltoleranz (engl. *Partition Tolerance*) nicht gleichzeitig zu jedem beliebigen Zeitpunkt erfüllt sein können. [@Meier2016]

- __Konsistenz (engl. *Consistency*)__
  "Wenn eine Transaktion auf einer verteilten Datenbank mit replizierten Knoten Daten verändert, erhalten alle lesenden Transaktionen den aktuellen Zustand, egal über welchen der Knoten sie zugreifen." 

- __Verfügbarkeit (engl. *Availability*)__
  "Unter Verfügbarkeit versteht man einen ununterbrochenen Betrieb der laufenden Anwendung und akzeptable Antwortzeiten." 

- __Ausfalltoleranz (engl. *Partition Tolerance*)__
  "Fällt ein Knoten in einem replizierten Rechnernetzwerk oder eine Verbindung zwischen einzelnen Knoten aus, so hat das keinen Einfluss auf das Gesamtsystem. Zudem lassen sich jederzeit Knoten ohne Unterbruch des Betriebs einfügen oder wegnehmen." 

  _vgl. [@Meier2016]_

![Möglichkeiten der Einordnung im CAP Theorem [@Meier2016]](images/CAP-Theorem.png){#fig:CAP}

### Einordnung von Cassandra im CAP Theorem

In der Realität ist es nur schwer möglich Datenhaltungssysteme in nur eine, der in [Abbildung @fig:CAP] zusehenden, Möglichkeiten des CAP Theorem einzuordnen. Apache Cassandra wird in der Literatur meist in den Bereich der AP-Datenhaltungssysteme eingeordnet, da aufgrund der verteilten Architektur von Cassandra Dateninkonsistenzen in Clustern auftreten können, die durch nicht erreichbare Knoten verursacht werden [@Sahu2015]. Cassandra selbst versucht automatisch die entstandene "Entropie" des Clusters zu reduzieren. Dennoch wird  Administratoren empfohlen regelmäßig sicherstellen, dass die Daten über alle Knoten hinweg konsistent sind. Dafür bietet Cassandra von Haus aus ein Repairskript an, mit dem Ziel, die Knoten möglichst konsistent zu halten. [@DataStax]

Demnach erfülle Cassandra also nur bedingt den Punkt der Konsistenz. Cassandra bietet jedoch eine konfigurierbare" "eventual consistency". So können bei Cassandra für einzelne Lese- und Schreiboperationen Konsistenzlevel gewählt werden. Diese Konsistenzlevel werden im Laufe dieser Arbeit genauer erläutert. [@MIshra2014]

## Wide-Column Store

Cassandra zählt zu den sogenannten Wide-Column Stores. Wide-Column Stores sind schemafrei. Wide-Column Stores sollte nicht mit spaltenorientierten Datenbanken aus dem Bereich der relationalen Datenbanken verwechselt werden. Diese spaltenorientierten relationalen Datenbanken sind grundlegend verschieden zu Wide-Column Stores. [@DB-EnginesEncyclopedia]

Das Datenmodell eines Wide-Column Stores unterscheidet sich erheblich von dem eines RDBMS. Es existieren zwar auch die Begriffe Zeile (Row) und Spalte (Column), diese besitzen hier jedoch andere Eigenschaften. [@Jansing]

Eine Spalte ist die kleinste logische Einheit in Cassandra. Jede Spalte besteht aus einem Spaltennamen, einem Wert und einem Zeitstempel. [@Jansing]

![Cassandra Datenmodell](images/cassandra_data_model.png){#fig:test width=70%}

Eine Zeile besteht hier aus Spalten und einem Schlüssel der die Zeile eindeutig identifiziert, wie in [Abbildung @fig:test] zu sehen [@Jansing]. Das besondere ist, dass unterschiedliche Zeilen eine unterschiedliche Anzahl von Spalten haben können [@Jansing]. Wie der Name Wide-Column Store schon impliziert, kann eine Zeile sehr viele Spalten haben. Beispielsweise sind bis zu einer Millionen Spalten, oder auch mehr möglich [@Lakshman2009]. Da der Spaltenname, sowie der Reihenname frei wählbar sind, werden Wide-Columns Stores auch als zweidimensionaler Key/Value Store bezeichnet [@DB-EnginesEncyclopedia].

Eine Menge an Zeilen wird in einer Art Tabelle zusammengefasst, in Cassandra Column-Family genannt. Mehrere Column-Families werden in einem Keyspace zusammengefasst.

## Vergleich zu anderen NoSQL-Datenbanken

Neben Wide-Column Stores gibt es noch andere NoSQL-Datenbanktypen. Diese sind zum Beispiel Key/Value, dokumentenorientierte oder Graph-Datenbanken. Diese vier Datenbanktypen werden auch als Core-Datenbanken bezeichnet. Die bekanntesten Datenbanksysteme sind Redis (Key/Value-Datenbanken), MongoDB (dokumentenorientierte Datenbank) und Neo4J (Graph-Datenbank). Neben den soeben genannten NoSQL-Datenbanktypen gibt es noch weitere Typen, die allerdings oftmals nicht sehr verbreitet sind, da sie nur für Einzelfälle genutzt werden. Diese werden Soft-Datenbanken genannt und zu ihnen zählen zum Beispiel XML, Gitter oder Objekt-Datenbanken.[@Fasel2016;@Muller2014]

### Key/Value-Datenbanken

Key/Value-Datenbanken speichern Schlüssel/Wert-Paare und sind meistens besonders schnell, weil sie diese Paare im Arbeitsspeicher halten und somit eine schnelle Antwortzeit besitzen. Jedoch gibt es auch Key/Value-Datenbanken, die die Paare auf eine Festplatte schreiben und von dort auch wieder lesen.[@Fasel2016;@Meier2016]

### Dokumentenorientierte Datenbanken

Dokumentenorientierte Datenbanken speichern Datensätze als sogenannte Dokumente ab. Die Dokumente beinhalten die Informationen als Schlüssel/Wert-Paare. Sie haben keine Verbindung zu Dateien auf einem Computer, sondern sind lediglich zum speichern von Informationen zu einem bestimmten Datensatz gedacht. Diese Dokumente werden in Kollektionen abgespeichert, wodurch eine bessere Unterteilung erreicht wird.[@Fasel2016;@Edward2015;@Harrison2015;@Meier2016]

### Wide-Column Store

Wide-Column Stores speichern Daten ebenfalls als Schlüssel/Wert-Paare ab, jedoch mit einem zusätzlichen Schlüssel, der für die Versionierung der Daten zuständig ist. Dieser Schlüssel soll die aktuellsten Daten markieren, weswegen dieser meistens ein Zeitstempel ist. Ein Datensatz wird als Reihe dargestellt, bei der die dazugehörigen Schlüssel/Wert-Paare eingetragen sind.[@Fasel2016;@Meier2016]

### Graph-Datenbanken

Graph-Datenbanken bestehen im Gegensatz zu dokumentenorientierten Datenbanken nicht aus Dokumenten, sondern aus Knoten und Kanten. Knoten bilden die Datensätze ab. Hier werden die Daten, genau wie in dokumentenorientierten Datenbanken, als Schlüssel/Wert-Paare gespeichert. Zwischen den einzelnen Knoten liegen Kanten, die die Knoten verbinden. Die Kanten können ebenfalls Schlüssel/Wert-Paare als Informationen besitzen, sind jedoch nicht als Datenspeicher gedacht. Sie sind dafür gedacht, die Relation zwischen zwei Knoten darzustellen.[@Fasel2016;@Jordan2014;@Meier2016]

### Unterschiede

Der Unterschied zwischen den NoSQL-Datenbanktypen liegt im Verwendungszweck. Während Cassandra, also Wide-Column Stores, oftmals bei stark veränderlichen Daten genutzt werden, werden Key/Value-Datenbanken bei einfachen Daten, wie zum Beispiel Anwendungsvariablen, eingesetzt. Außerdem können die Daten in Key/Value-Datenbanken keine Verbindung untereinander haben, was eine relationale Logik außerhalb der Datenbank voraussetzt. Auch Wide-Column Stores können keine relationale Logik beinhalten, da es keine Fremdschlüssel gibt. Dokumentenorientierte Datenbanken sind für Daten gedacht, die kein eindeutiges Schema besitzen, wie zum Beispiel ein Adressbuch. Aber auch bei Daten, die einem Schema folgen, aber Lücken besitzen, sind sie geeignet. Sie sind schemafrei, was bedeutet, dass nicht jedes Dokument demselben Schema folgen muss. Hier sind außerdem einfache Relationen zwischen Dokumenten möglich. Auch Wide-Column Stores sind schemafrei, was ihnen ermöglicht, Datensätze mit unterschiedlichen Schemata zu beinhalten. Graph-Datenbanken sind ebenfalls für Daten gedacht, die keinem Schema unterliegen. Jedoch sind Graph-Datenbanken in erster Linie für Daten gedacht, die stark untereinander zusammenhängen. Dies wäre zum Beispiel bei einer Organisationsstruktur eines Unternehmens der Fall.[@Fasel2016;@Meier2016]

Jeder NoSQL-Datenbanktyp hat seinen speziellen Anwendungsfall, in dem er am besten eingesetzt werden kann und dort einen Vorteil gegenüber anderen NoSQL-Datenbanktypen besitzt.  


## Horizontale Skalierung

Während RDBMS Systeme vertikale Skalierung unterstützen, also das vorhandene Server aufgerüstet werden, bieten NoSQL-Datenbanken eine einfache und kostengünstige horizontale Skalierung an. Hierbei werden einfach weitere Server in den Verbund aufgenommen. 

Cassandra basiert auf einem Peer-To-Peer Modell. Das bedeutet, dass jeder Knoten die gleiche Rolle hat und Cassandra dadurch dezentralisiert ist. Es gibt keinen Knoten, der als Master-Knoten fungiert und die Verteilung der Daten steuert. Stattdessen wird bei einer Anfrage temporär ein zufälliger Knoten ausgewählt, der sogenannte Koordinator. Dieser Koordinator bearbeitet die Anfrage und wird anschließend wieder zu einem normalen Knoten. Auch wenn ein neuer Knoten der Datenbank hinzugefügt wird, wird ein Knoten als Quelle ausgewählt, von dem der neue Knoten die Daten erhalten soll. Grundsätzlich kann jeder Knoten diese Aufgaben durchführen, wodurch ein Single Point of Failure vermieden wird.[@MIshra2014; @CodecentricAG]

Eine horizontale Skalierung ist bei diesem Ansatz fast linear möglich. Die doppelte Anzahl an Knoten kann als die doppelte Anzahl an Anfragen verarbeiten und es kann auch die doppelte Menge an Daten gespeichert werden. [@CodecentricAG]

Ein praktisches Beispiel für die horizontale Skalierung bietet Netflix. Wie in [Abbildung @fig:HorizontalScalingNetflix] zu sehen, ist es Netflix möglich mit einem Cluster von 50 Servern eine Schreibrate von ca. 174.000 Schreiboperationen pro Sekunde. Bei einem Cluster mit 300 Servern wird eine Schreibrate von ca. 1.100.000 Schreiboperationen pro Sekunde [@DataStaxc].

![Horizontale Skalierung bei Netflix [@Cockcroft2011]](images/HorizontalScalingNetflix.png){#fig:HorizontalScalingNetflix width=70%}

Ein bestehendes Cassandra Cluster horizontal skalieren, also weitere Knoten hinzuzufügen, erfolgt beispielweise durch das Starten eines Dockercontainers auf dem neuen Server mit nur einem Kommando. Dafür muss auf dem neuen Knoten das Cassandra Docker Image herunterladen und dann ausgeführt werden. Beim Startbefehl des Containers muss lediglich die Umgebungsvariable `CASSANDRA_SEEDS="<IP Adresse eines bestehenden Knoten>"` übergeben werden. Mit dem Start des Containers fügt sich der Knoten dann selbst in das Cassandra Cluster ein. [@DiTullio2016a]

### Das Gossip-Protokoll

Aufgrund des Fehlens eines Master-Knoten, der die Kommunikation zwischen seinen Slave-Knoten und dem Cluster steuert, muss jeder Knoten zu jeder Zeit über den Status des Clusters Bescheid wissen. Die Knoten schicken in jeder Sekunde zu höchstens drei anderen Knoten Informationen über ihren Status und den Status von jedem Knoten, über den sie Bescheid wissen. Dadurch weiß durchgehend jeder Knoten über den Status des Clusters Bescheid. Vorrangiges Ziel ist es, Ausfälle von Knoten zu bemerken und die Aufgaben auf die noch verfügbaren Knoten zu verteilen.[@Harrison2015]

## Datenspeicherung

Der Speicherbereich wird in Cassandra über alle Knoten verteilt. Hierbei bekommt jeder Knoten einen Teil des Bereiches. Der Bereich orientiert sich an Hash-Values. Diese Hash-Werte werden aus den Reihenschlüsseln errechnet und anschließend werden die dazugehörigen Daten auf den für den Speicherbereich eingeteilten Knoten gespeichert.[@Harrison2015]

Beispiel: Die Datenbank hat vier Knoten. Der Speicherbereich besitzt die Hash-Werte von -2^63^ bis 2^63^ - 1. Diese Werte werden gleichmäßig auf alle vier Knoten verteilt. Knoten A bekommt die Werte von -2^63^ bis -2^63^ / 2, der Knoten B bekommt die Werte von -2^63^ / 2 bis 0, der Knoten C bekommt die Werte von 0 bis 2^63^ / 2 und der Knoten D bekommt die Werte von 2^63^ / 2 bis 2^63^.[@Harrison2015]

Grundsätzlich wird der Speicherbereich als ein Kreis dargestellt. [Abbildung @fig:hashRanges] zeigt, wie ein Datensatz anhand des Reihenschlüssels in einem bestimmten Knoten gespeichert wird.

![Speicherung eines Datensatzes in einem Knoten [@Harrison2015]](images/hashRanges.png){#fig:hashRanges}

Wenn ein neuer Knoten hinzugefügt wird, müsste der Speicherbereich für alle anderen Knoten angepasst werden, um ein ausbalanciertes Cluster zu erhalten. Da das allerdings sehr teuer ist, kann auch nur von einem Knoten der Speicherbereich geteilt werden, um dem neuen Knoten Speicherbereich zuzuteilen. Dadurch ist das Cluster allerdings nicht mehr ausbalanciert. Um dem vorzubeugen werden sogenannte virtuelle Knoten benutzt. Der Speicherbereich wird in diese virtuellen Knoten eingeteilt und unter den physischen Knoten gleichmäßig verteilt. Dadurch ist es ohne großen Aufwand möglich, ein ausbalanciertes Cluster zu erhalten.[@Harrison2015]

### Verteilstrategien für Partitionen

Cassandra besitzt drei verschiedene Strategien, um Daten auf den Knoten zu verteilen:

* **RandomPartitioner** - Hier wird der MD5 Hash-Algorithmus genutzt, um die Daten gleichmäßig über alle Knoten zu verteilen.
* **Murmur3Partitioner** - Das ist der Standard-Strategie. Hier wird anstatt der MD5- Algorithmus der Murmur3-Algorithmus genutzt, um die Daten zu verteilen.
* **ByteOrderedPartitioner** - Mit dieser Strategie werden die Daten anhand des Reihenschlüssels im Cluster verteilt. Allerdings kann die Verteilung der Daten ungleichmäßig erfolgen, wodurch einige Knoten mehr Daten, als andere Knoten haben können.[@MIshra2014]

### Replikation der Daten im Cluster

Cassandra bietet bereits von Haus aus eine Replikation der Daten an. Diese Replikation wird bei Cassandra auf der Ebene des Keyspace definiert, sodass verschiedene Keyspaces verschiedene Replikationslevel besitzen können. [@DataStax]

Um die Replikation zu aktivieren, muss bei der Erstellung eines Keyspace der sogenannte Replikationsfaktor und eine Replikationsverfahren spezifiziert werden. Der Replikationsfaktor sagt aus, auf wie vielen zusätzlichen Knoten eine Replikation abgelegt werden soll. Ist der Replikationsfaktor gleich eins, so wird auf einem zusätzlichen Knoten eine Replikation abgelegt. [@DataStax]

Cassandra bietet für die Replikation drei unterschiedliche Verfahren an, die je nach Eigenschaften des Clusters gewählt werden sollten: 

* __Local Strategy__
  Die `Local Strategy` wird lediglich für interne Zwecke von `system` und vom `system_auth`Keyspace genutzt. Cassandra unterbindet das Erstellen eines Keyspace mit dem Replikationsverfahren `Local Strategy`. 

* __Simple Strategy__
  Die `Simple Strategy` sollte genutzt werden, wenn die Knoten des Clusters auf einem Rack verteilt oder auf mehreren Racks in einem Data-Center stehen. Auch wenn Cassandra im Local Mode ausgeführt wird sollte diese Strategie genutzt werden. 

* __Network Topology Strategy__
  Die `Network Topology Strategy` soll genutzt werden, wenn mehrere Knoten in unterschiedlichen Data-Center stehen. Hier kann für jedes Data-Center ein eigener Replikationsfaktor angegeben werden. 

  _vgl. [@MIshra2014]_

In [Quellcode @lst:KeysapceWithReplicationSimpleStrategy] ist der CQL Befehl zum Erstellen eines Keyspace mit den Namen `keyspace` mit `Simple Strategy` als Replikationsverfahren und einem Replikationsfaktor von drei dargestellt. [@MIshra2014]

\pagebreak

```cql
CREATE KEYSPACE keyspace WITH replication = {'class': 'SimpleStrategy', 'replication_factor' : 3};
```
: CQL Befehl zum Erstellen eines Keyspace mit SimpleStrategy {#lst:KeysapceWithReplicationSimpleStrategy}

In [Quellcode @lst:KeysapceWithReplicationNetworkTopologyStrategy] ist der CQL Befehl zum Erstellen eines Keyspace mit dem Namen `keyspace` mit `NetworkTopologyStrategy` als Replikationsverfahren und einem Replikationsfaktor von zwei für Data-Center 1 und einem Replikationsfaktor von drei für Datacenter 2 dargestellt. `dc1` und `dc2` stellen hierbei die Namen von Datacenter 1 und Datacenter 2 dar. [@MIshra2014]


```cql
CREATE KEYSPACE keyspace WITH replication = {'class': 'NetworkTopologyStrategy', 'dc1' : 2, 'dc2' : 3};
```
: CQL Befehl zum Erstellen eines Keyspace mit NetworkTopologyStrategy {#lst:KeysapceWithReplicationNetworkTopologyStrategy}

### Schreib-Konsistenz

Wenn die Daten erfolgreich auf die Replika transferiert wurden, bevor die Schreib-Anfrage akzeptiert wurde, gelten die Daten als schreib-konsistent. Hier sind mehrere Konsistenzstufen möglich:

* __ANY__ - Schreib-Anfragen müssen erfolgreich auf mindestens irgendeine Replika geschrieben werden. Wenn keine Repliken verfügbar sind, werden die Informationen in dem Koordinator-Knoten zwischengespeichert und ist nicht für Lese-Anfragen verfügbar, bis mindestens eine Replika wieder verfügbar ist. ANY besitzt die geringste Konsistenzstufe.
* __ONE|TWO|THREE__ - Schreib-Anfragen müssen erfolgreich auf eine|zwei|drei Repliken geschrieben werden.
* __QUORUM__ - Schreib-Anfragen müssen erfolgreich auf eine ausgewählte Gruppe von Repliken geschrieben werden.
* __LOCAL_QUORUM__ - Schreib-Anfragen müssen erfolgreich auf eine ausgewählte Gruppe von Repliken geschrieben werden, welche sich innerhalb desselben Datencenters befindet, wie der Koordinator-Knoten.
* __EACH_QUORUM__ - Schreib-Anfragen müssen erfolgreich auf mehrere ausgewählte Gruppen von Repliken geschrieben werden.
* __ALL__ - Schreib-Anfragen müssen erfolgreich auf alle Repliken geschrieben werden, wodurch die höchste Konsistenz erreicht wird.
* __SERIAL__ - Schreib-Anfragen müssen entweder bei allen Repliken innerhalb einer Gruppe in das Commit-Log und die Speichertabelle, oder bei keinen Repliken der Gruppe, geschrieben werden.[@MIshra2014]

\pagebreak

### Lese-Konsistenz

Um stets die aktuellsten Daten zu erhalten und Inkonsistenzen vorzubeugen, gibt es verschiedene Konsistenzstufen bei Lese-Anfragen:

* __ONE|TWO|THREE__ - Die neusten Daten werden von dem|den zwei|den drei zum Koordinator-Knoten nächstgelegenen Knoten geliefert. ONE ist die geringste Konsistenzstufe.
* __QUORUM__ - Die neusten Daten aus einer ausgewählten Gruppe von Knoten werden zurück geliefert.
* __LOCAL_QUORUM__ - Die neusten Daten aus einer ausgewählten Gruppe von Knoten, die sich im selben Datencenter befinden, werden zurück geliefert.
* __EACH_QUORUM__ - Die neusten Daten aus allen Gruppen von Knoten werden zurück geliefert.
* __ALL__ - Die neusten Daten aus allen Knoten werden zurück geliefert. Hier wird die höchste Konsistenzstufe erreicht.
* __SERIAL__ - Die neusten Daten, die bereits geschrieben wurden, oder gerade verarbeitet werden.

### Interne Datenspeicherung eines Knoten {#sec:cpt-save-cassandra}
Cassandra bietet durch seine spezielle Speicherung der Daten auf einem Knoten hohe Geschwindigkeit beim Schreiben [@TeddyMa]. Der Durchsatz beim Schreiben ist dabei 8 mal so hoch wie bei einer HBase Datenbank bei selber Hardware [@DataStaxc].

![Ablaufplan eines Schreibprozesses innerhalb eines Knoten [@PhilippLanger2017]](images/write-path.png){#fig:WritePath}

Wenn ein Knoten vom Koordinatorknoten eine Schreibanfrage bekommt, schreibt der Knoten wie in [Abbildung @fig:WritePath] zu sehen, zuerst die Daten in den Commit-Log auf der Festplatte, um die Haltbarkeit zu garaniteren. Nachfolgend werden die Daten vom Knoten in die Memtable geschrieben. Dies ist eine Tabelle die im Arbeitsspeicher des Knoten gehalten wird. Der Commitlog stellt sicher, dass falls ein Knoten abstürzten sollte die Memtable wiederhergestellt werden kann. Ist das Schreiben in die Memtable erfolgt, so wird dem Koordinator bestätigt, dass die Daten geschrieben wurde. [@PhilippLanger2017]

Die restliche Verarbeitung erfolgt nun asynchron, was auch der Grund für die Schnelligkeit von Cassandra ist. Erreicht die Memtable nun ein bestimmtes Limit, so wird der Inhalt der Memtable auf die Festplatte verlagert. Diese Verlagerung der Memtable auf die Festplatte kann auch durch das Überschreiten der Commitlog-Größe ausgelöst werden. Das Übertragen der Memtable beginnt mit dem Sortieren deren Inhalte nach dem Partitionsschlüssel. Anschließend werden die Daten sequenziell in eine Sorted String Table (SSTable) auf der Festplatte geschrieben. Nach dem Schreibprozess der SSTable wird diese zu einer unveränderbaren Datei [@DataStax]. Alle Daten die erfolgreich in die SSTable geschrieben wurden, werden aus der Memtable, sowie dem Commitlog entfernt. [@PhilippLanger2017]

#### Compaction

Durch diesen Schreibprozess können für eine Tabelle in Cassandra viele SSTables entstehen. Diese werden regelmäßig durch einen Prozess namens Compaction zu neuen SSTables vereinigt, um schnellere Lesezugriffe zu ermöglichen [@DataStax]. Da Casandra bei einem `UPDATE` Befehl nicht die Daten überschreibt, sondern einen neuen Eintrag mit einem neueren Zeitstempel anlegt, können die Daten eines Eintrag über mehrere SSTables verteilt sein. Auch `DELETE` Befehle werden nicht direkt ausgeführt, da die SSTables unveränderbar sind. Stattdessen markiert Cassandra die Zeile mit einem Thombstone (dt. Grabstein). Der Compaction Prozess fügt nun die Daten mit selben Partitionkey jeder SSTable anhand des neusten Zeitstempel zusammen. Nachfolgend werden die mit einem Thombstone markierten Einträge entfernt alles zu einer neuen SSTable zusammengefügt, die nach dem Partitionkey sortiert ist. Die alten SSTables werden nun gelöscht. [@DataStaxa]

Seit Cassandra 2.1 werden die alten SSTables inkrementell ersetzt, sodass bereits während dem Zusammenfügen und Schreiben der neuen SSTable aus dieser gelesen werden kann, noch bevor der Compaction Prozess komplett abgeschlossen wurde. [@DataStaxa]

## Abfragesprache Cassandra Query Language (CQL)

Cassandra besitzt eine Abfragesprache, die Cassandra Query Language, kurz auch CQL genannt. CQL ist an SQL angelegt und weist viele Ähnlichkeiten auf. So teilen sich SQL und CQL das Konzept von Tabellen (Column Families), Zeilen und Spalten. CQL kennt auch die Befehle `CREATE TABLE` zum Erstellen von Tabellen, `SELECT` zum Anzeigen von Daten und `INSERT`, `UPDATE` und `DELETE` zum manipulieren der Daten. Auch kann die `WHERE` Klausel genutzt werden um die Ergebnismenge einzuschränken. [@Jansing]

Um CQL-Anfrage an Cassandra schicken, wird die mitgelieferte CQL Shell `cqlsh` genutzt. Ebenso gibt es Programmierschnittstellen für verschiedene Programmiersprachen. [@Jansing]

Auf die Einschränkungen und Schwächen von CQL wird in [Kapitel @sec:cpt-cql-schwach] genauer eingegangen.






