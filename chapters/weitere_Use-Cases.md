# Anwendungsfälle Cassandra {#sec:cpt-cassanda}
Für die Anwendungsfälle von Cassandra sind insbesondere die drei nachfolgend erläuterten Faktoren von entscheidenden Interesse:

- die __hohe Verfügbarkeit__ von Cassandra, die durch die verteilte Architektur von Cassandra zu Stande kommt.
- die __lineare Skalierung__, durch die es einfach ist, weitere Maschinen oder Nodes zu Cassandra hinzuzufügen.
- eine __vorhersagbare Performance__, in dem Cassandra vorhersagbar skaliert, sodass sichergestellt werden kann, dass eine geringe Latenz und eine hohe Performance bei jeder Größe zu erwarten ist [@DataStaxAcademyUCFraud].

Für den Einsatz lassen sich fünf verschiedene Anwendungsbereiche besonders ermitteln:

1. Im Bereich __Fraud Detection__, also Betrugserkennung, wird Cassandra besonders im Bankenumfeld eingesetzt. [@DataStaxAcademyUCFraud]. Dabei ist es schwierig solche Betrugsfälle zu erkennen, das hat vor allem drei Ursachen: Zum einen müssen schnell (durch die Echtzeitanforderungen der Transaktionen) Terrabytes von Daten durchsucht werden. Andererseits müssen diese Signale erstmal erkannt werden, d.h. die Betrugsfälle müssen aus dem Rauschen (also den vielen anderen Daten) ermittelt werden. Außerdem wird die Art und Weise des Betrugs von den Betrügern schnell geändert und in der Regel durch valide Transaktionen verschleiert. Dadurch ist eine Suche nach "Nadel im Heuhaufen" so schwierig [@Heath2017].
Nach DataStax verwendet zum Beispiel Ebay DataStax Enterprise (welches auf Cassandra basiert), um die Betrugsfallerkennung bei den Transaktionen durchzuführen. Außerdem werden Protextwise, ACI Universal Payment und Nextgate genannt [@DataStaxFraud].
2. Im __Messaging__-Umfeld, da die Nachrichten ein große Datenmenge produzieren. Diese Datenmengen müssen dann in einer Datenbank gespeichert werden und die Nutzer sollen jeweils nicht lange warten müssen, bevor die letzten Nachrichten geladen werden [@DataStaxAcademyUCMessaging]. Aktiv eingesetzt wird Cassandra in diesem Anwendungsfall zum Beispiel von Discord [@Vishnevskiy2017].
3. Als weiterer Anwendungsfall gilt die __Personalisierung und Empfehlungsgenerierung__. Denn heute wird bei immer mehr Unternehmen (z.B. Netflix, Amazon,...) personalisierte Empfehlungen erstellt und auch vom Kunden erwünscht. Allerdings müssen auch dabei große Datenmengen in einer sehr kurzen Zeit analysiert und ausgewertet werden [@DataStaxAcademyUCPersonalization]. Aktiv angewendet wird dies zum Beispiel bei Spotify. Dabei gibt es Millionen Songs für Millionen Kunden. Dabei können von den Kunden ganz verschiedene Playlists erstellt werden. Spotify versucht daher, den Kunden Empfehlungen an Hand des Verhaltens für bestimmte Playlists zu geben. Um dies zu gewährleisten, wird neben Cassandra HDFS; Crunch, Storm und Kafka eingesetzt [@DataStaxAcademyUCPersonalization].
4. Für __Produktkataloge, Playlisten und Warenkörbe__ wird Cassandra ebenfalls eingesetzt. Dabei geht es darum, aus einer sehr großen Datenmenge die Daten zu filtern und möglichst schnell für den Kunden verfügbar zu machen. Insbesondere bei Shopping Angeboten sollen die Angebote auf den hinteren Seiten auch noch schnell laden. Bei Produktkatalogen sollen Änderungen möglichst schnell für den Kunden sichtbar sein [@DataStaxUCPlaylist].
5. Zur Sammlung von Daten im Bereich __Internet of Things__. Durch die zunehmende Steigerung der internetfähigen Sensoren im Haushalt, ist eine immer größer werdende Datenflut zu verarbeiten. Dabei zeichnen sich IoT-Geräte meist durch eine hohe Anzahl an Schreib-Operationen aus (d.h. sie melden die Daten an die jeweiligen Unternehmen, die dann Analysen fahren). Als Beispiel wird von DataStax i2O genannt, eine Firma, die Wassersensoren einsetzt, um die Wassernetze zu überwachen [@DataStaxUCIoT].

Neben den bereits angesprochenen Unternehmen sollen im Folgenden weitere spezifische Anwendungsfälle betrachtet werden. Davor allerdings soll noch ein Blick auf zwei Unternehmen geworfen werden: Zum einen auf Facebook, welches Cassandra entwickelt hat und DataStax, das vorwiegend den kostenpflichtigen Support für Cassandra anbietet:
Bei Facebook wurde Cassandra bis 2011 eingesetzt. Im Sommer 2011 wurde dies "durch eine Lösung aus der In-Memory-Datenbank Apache HBase, HDFS und Haystack ersetzt." [@BigdataCassGrundlagen190318]
DataStax ist ein Unternehmen, dessen "gesamtes Geschäftsmodell [sich] auf kostenpflichtigen Support für das Casandra-Apache-Open-Source-Projekt und Cassandra-Trainings" [@BigdataCassGrundlagen190318] begründet. Inzwischen ist DataStax der Hauptentwickler von Cassandra.

### Netflix
Netflix hat 2010 angefangen, ihre Daten in AWS (Amazon Web Services) umzuziehen. Vor diesem Umzug wurden die Daten in einer Oracle-Datenbank gespeichert, wobei auf Grund der Nutzerzahlen die Limits bezüglich Netzwerkverkehr und Kapazität erreicht wurden. Außerdem war das verbundene Datencenter ein Single-Point-of-failure, in dem die Anfragen auf eine zentrale SQL-Datenbank ausgeführt wurden. Ein weiteren Punkt bildete der Fakt, dass Netflix eine 10-minütige Downtime benötigte, um neue Schemata einzupflegen.
Mit der jetzigen Implementierung sind mehr als zehn Millionen Transaktionen pro Sekunde möglich. An einem normalen Tag verarbeitet Netflix durchschnittlich mehr als 2,1 Milliarden Schreib- und mehr als 3,4 Milliarden Leseoperationen. Die Daten sind in verschiedene Data-Cluster aufgeteilt und Netflix kann innerhalb von 10 Minuten ein neues Cluster überall auf der Welt eröffnen. [@DataStaxNetflix].

### eBay
eBay als weltgrößte Onlineplattform mit ca. 112 Millionen aktiven Kunden stellen die Milliarden an Schreib- und Leseoperationen  vor große Herausforderungen. Um dieser Herr zu werden, wurde auf Cassandra zurückgegriffen. Pro Tag laufen sechs Milliarden Schreiboperationen und fünf Milliarden Leseoperationen auf den Cassandra-Clustern. Außerdem wird Cassandra bzw. Funktionalitäten von DataStax-Enterprise zum Loggen und Tracken der mobilen Aktivitäten, zur Betrugserkennung sowie zur Log-Analyse eingesetzt. [@DataStax2014]

### Microsoft
Microsoft nutzt zur Unterstützung von Office 365 Cassandra. Das Projekt, als ein 40-Knoten Projekt gestartet, inzwischen hat sich jedoch die Zahl der Knoten verzehnfacht. Dabei werden ca. 400.000 Schreiboperationen pro Sekunde verarbeitet, wobei Microsoft auch besonders Wert auf die Latenzzeiten, zwischen 20 und 30 ms, gelegt hat. [@DataStax2017]

### Discord
Discord ist eine Nachrichten-Plattform. Darauf können verschiedene Server erstellt werden, die wiederrum verschiedene Sektionen, sogenannte Channels, haben können. Dabei unterstützt Discord sowohl Sprach- als auch Textchat.

Discord hat am 13. Januar 2017 einen Artikel unter [@Vishnevskiy2017] veröffentlicht, in dem der Umstieg auf Cassandra erklärt wurde. Im Folgenden ist dieser Umstieg näher erläutert.

#### Welche Besonderheiten hat Discord?
Auf Grund der verschiedenen Einsatzszenarien von Discord kommt es zu unterschiedlichen Read/Write-Anforderungen:

- Die Leseoperationen waren komplett zufällig, auch wenn sich Lese- und Schreiboperationen in Summe circa die Waage hielten.
- Auf Servern, bei denen vorwiegend der Sprachchat verwendet wird, werden quasi keine Nachrichten gesendet. Es werden damit nicht mehr als ein oder zwei Nachrichten in einigen Tagen gesendet. Da diese Nachrichten allerdings, auf Grund der Vielzahl anderer Nachrichten, nicht mehr im Cache liegen können, kann es hier zu Performanceproblemen kommen.
- Dahingegen gibt es ebenfalls private Server mit hohem Textaufkommen, bei denen Tausende bis hin zu Millionen Nachrichten im Jahr verschickt werden. Dabei sind vor allem die aktuellen Daten wichtig, die vor kurzem gesendet wurden. Allerdings sind die Daten auf Grund der geringen Mitgliederzahlen eher nicht im Cache vorhanden.
- Außerdem gibt es große, öffentliche Server. Dort sind tausende Mitglieder, die wiederrum tausende Nachrichten im Jahr senden. Da diese Daten häufig aktualisiert und ebenso häufig angefragt werden, sind diese im Cache.
- Dazu kommen noch eine Reihe an Anforderungen von Discord selbst, die wiederrum zufälliges Lesen nach sich ziehen: Die Erwähnungen der letzten Tage sollten lesbar sein, außerdem soll zu diesem Punkt gesprungen werden können. Ebenso problematisch ist eine full-text-Suche.

#### Abgeleitete Anforderungen
Aus dem Verhalten wurden verschiedene Anforderungen abgeleitet:

- Lineare Skalierbarkeit: Die Daten sollen später nicht neu verteilt werden müssen.
- Automatisches Failover: Bei Problemen sollen automatisch Notfallprogramme gestartet werden.
- Geringe Wartungstätigkeiten: Es soll einmal aufgesetzt werden und danach funktionieren.
- Proven to work: Bereits andere sollen die Funktionalitäten bestätigen können.
- Vorhersagbare performance: Alarme sollen ausgelöst werden, wenn das 95%-Quantil von 80ms erreicht wird.
- Kein Blob-Speicher: Durch die hohe Anzahl an Nachrichten können diese nicht konstant deserialisiert und angehangen werden.
- Open Source: Es sollte keine extra Firma eingekauft werden müssen.

Da nur Cassandra alle Anforderungen erfüllen konnte, wurde diese als Datenbank-System ausgesucht [@Vishnevskiy2017]. Inzwischen laufen die Nachrichten von Discord über eine Cassandra-Datenbank.

# Anwendungsfälle Spark {#sec:cpt-use-spark}
Im Folgenden werden zuerst die grundsätzlichen Anwendungsfälle erklärt, und dann auf Beispiele in Unternehmen eingegangen.

## Grundsätzliche Anforderungen

Spark ist für verschiedene Anwendungsfälle nützlich, dabei lassen sich jedoch die in den vier nachfolgenden Subkapiteln nach [@Amster2017] erläuterten Anforderungen besonders herausstellen.

### Daten streamen 
Den ersten Hauptanwendungsfall bietet Spark mittels __Daten streamen__. Es geht dabei darum, dass die Daten, die in einem Geschäftsprozess gewonnen werden, in Echtzeit zu übertragen und ausgewertet werden. In der heutigen Geschäftswelt wird Spark Streaming besonders bei folgenden Aufgaben eingesetzt:

- _im ETL-Prozess (Extract, Transform, Load)_, also dem Ladeprozess ins Datawarehouse. Dabei müssen Daten zuerst gelesen, anschließend in ein datenbankpassendes Format transformiert und anschließend ins Datawarehouse geschrieben.
- _zur Datenanreicherung_, indem Live-Daten mit historischen Daten angereicht werden. Das führt dazu, dass die Analysen besser werden. Dabei werden zum Beispiel bei Onlinewerbung das aktuelle Nutzerverhalten mit historischem Verhalten angereichert, um somit bessere personalisierte Werbung ausliefern zu können.
- _zur Trigger-Ereignis-Erkennung_, wobei diese Ereignisse selten ausgeführtes, oder generell ungewöhnliches Verhalten des Nutzers ist. Diese Verhaltensweisen können dann auf potentiell schwerwiegende Probleme im System hinweisen.
- _zur komplexen Session-Analyse_, bei der das Verhalten der Nutzer in Echtzeit analysiert wird. Dabei werden die einzelnen Ereignisse zuerst gruppiert (i.d.R. nach dem Status), und danach analysiert. Dieser Stream bildet auch die Grundlage, um die Machine-Learning-Modelle konstant zu updaten. [@Amster2017]

### Machine Learning
Mittels der __Machine Learning__-Bibliothek werden tiefergehende Analysen umgesetzt, indem wiederholende Queries auf dem Datensatz ausgeführt werden, wodurch die Machine-Learning Algorithmen verarbeitet werden. Insbesondere zu erwähnen in diesem Kontext ist die skalierbare Machine Learning Library (MLlib). MLlib ist für den Einsatz in Umgebungen mit u.a. Clustering, classification, und dimensional reduction gedacht. Damit können verschieden Funktionsweisen, wie predictive intelligence, customer segemntation und sentiment analysis.
Außerdem kann im Bereich IDS/IPS Spark eingesetzt werden, indem die gesendeten Datenpakete in Echtzeit auf Spuren von Sicherheitsvorfällen untersucht werden kann. [@Amster2017]

### Interaktive Analyse
Die __Interaktive Analyse__ wird dafür genutzt, die Daten über eine Visualisierungsumgebung zu analysieren. Dabei ist besonders die Dauer der Verarbeitung wichtig, da SQL-on-Hadoop Tools, wie Hive oder Pig, zu langsam für die Verarbeitung sind. Spark ist schnell genug, explorative Abfragen ohne Sampling auszuführen. Damit kann Spark, mit einem geeigneten Visualisierungstool, zur Interaktiven Analyse eingesetzt werden. [@Amster2017]

### Fog Computing
__Fog Computing__ bezeichnet "verteilte Mikro-Recheneinheiten in der Nähe der Endgeräte" [@FogComputing] und wird besonders im Bereich IoT angewendet. Dabei wird eine große Menge an Daten produziert und verarbeitet. Daten dieser Mengen kann Cloud Computing nicht verarbeiten. Mit Fog Computing wird die Datenverarbeitung und -speicherung dezentralisiert, anstatt diese auf Netzwerkknoten auszuführen. Damit kommen auch neue Anforderungen an die dezentralisierten Daten: Es wird eine geringe Latenz, massive Parallelverarbeitung beim Machine Learning, und äußerst komplexe Graphen-Analyse-Algorithmen benötigt. Die bereits vorhandenen Komponenten bei Spark, z.B. Spark SQL als Echtzeit-Query-Tool, MLlib, GraphX und Spark Streaming qualifiziert sich Spark genau für diese Aufgaben. [@Amster2017]

## Anwendung in Unternehmen
Apache Spark wird in verschiedenen Wirtschaftssektoren angewendet, unter anderem:

- in der Finanzindustrie, insbesondere zur Betrugsfallerkennung (vergl. Trigger-Basierte-Events);
- im Bereich des e-commerce, zur Beobachtung der Realtime-Transaktionen;
- im Gesundheitssektor, zur Analyse der Patientendaten und generell zur Alarmierung bei kritischen Vitalwerten des Patienten;
- in der Unterhaltungsindustrie, insbesondere zur Empfehlungsgenerierung;
- im Reisesektor, ebenfalls zur Empfehlungsgenerierung.

