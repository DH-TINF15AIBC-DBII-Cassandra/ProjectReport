# Kombinierter Einsatz von Cassandra und Spark

Cassandra ist hervorragend zum performanten, verteilten Speichern von Daten geeignet. Allerdings hat die native CQL einige Nachteile hinsichtlich der unterstützten Abfragen, insbesondere im Vergleich zu den in \enquote{klassische} RDBMs mittels SQL möglichen [@DiTullio2016a]. Im Folgenden werden diese und die Verbindung von Cassandra und Spark zur Realisierung dieser Abfragen genauer dargestellt.


## Schwächen der Cassandra Query Language {#sec:cpt-cql-schwach}

CQL unterstützt einige grundlegende Querries, wie im ersten Kapitel dargestellt. Diese sind allerdings in einigen Punkten limitiert.
Für die folgenden Beispiele wird das in [Quellcode @lst:posts-sql-dd] bzw. [Quellcode @lst:posts-cql-dd] dargestellte Schema angenommen [@DiTullio2016a].

```sql
CREATE TABLE posts (
    username varchar(50) NOT NULL,
    creation DATE NOT NULL,
    content varchar(1000) NOT NULL,
    CONSTRAINT post_id PRIMARY KEY(username, creation)
);
```
: Das Schema der `Posts`-Tabelle in SQL {#lst:posts-sql-dd}

```cql
CREATE TABLE posts (
    username varchar,
    creation timeuuid,
    content varchar,
    PRIMARY KEY ((username), creation)
);
```
: Das Schema der `Posts`-Tabelle in CQL {#lst:posts-cql-dd}

Abgesehen von den verschiedenen Datentyp-Namen sind die Unterschiede beim Definieren des Primary Keys beachtenswert. Die doppelte Klammerung in der CQL beruht auf dem Verteilungs-Konzept: `username` ist der Partition-Key während `creation` der Clustering-Key ist. Die innere Klammer ermöglicht dabei, dass mehrere Spalten als zusammengesetzter (Composite) Partition- oder Clustering-Key benutzt werden können.
Weiterhin ist zu beachten, dass Cassandra keinen `NOT NULL` Constraint für Spalten unterstützt. Dies basiert darauf, dass Cassandra `NULL` Werte beim Markieren der zu löschenden Werte verwendet, wie im Einführungs-Kapitel zu Cassandra ausgeführt wurde.

Bei konkreten Abfragen ist zum einen für `ORDER BY` als Spalte nach der geordnet wird nur der Clustering-Key zulässig. Dadurch ist eine Abfrage wie in [Quellcode @lst:query-orderby] dargestellt in CQL nicht möglich, sie würde als `InvalidRequest` abgelehnt werden [@DiTullio2016a].
Grund dafür ist, dass beim Sortieren nach anderen Spalten entweder zufälliges Lesen oder ein in-memory Sortieren nötig wäre. Ersteres wird von Cassandra aus Performance-Gründen, letzteres aus Gründen der Einfachheit nicht implementiert [@DiTullio2016a].

```sql
SELECT * FROM posts
    WHERE username='Luca'
    ORDER BY content
;
```
: Eine `SELECT` Abfrage mit `WHERE` und `ORDER BY` Klausel in SQL {#lst:query-orderby}

Analog dazu ist es auch nicht ohne weitere Modifikationen am Tabellen-Schmea möglich, `WHERE` Konditionen auf anderen Spalten anzugeben, wie in [Quellcode @lst:query-where] dargestellt. Hierfür muss ein zusätzlicher Index angelegt werden oder diese Filterung muss seitens der Anwendung erfolgen.[@CQLManipulation2016]

```sql
SELECT * FROM posts
    WHERE content LIKE '%Oracle%'
;
```
: Eine `SELECT` Abfrage mit `WHERE` Klausel in SQL {#lst:query-where}

Des Weiteren muss bei Abfragen der partitionierende Teil des Primärschlüssels entweder auf einen (z.B. `WHERE username='Luca'`) oder einige (wenige) Werte (z.B: `WHERE username IN ('Flo', 'Joshua', 'Luca', 'Mauricio', 'Paul', 'Tim')`) festgelegt werden, wenn ein `ORDER BY` gewünscht ist. Eine Abfrage wie in [Quellcode @lst:query-oderby-whereless] dargestellt ist somit nicht möglich.
Für diese Abfrage müssten Daten von allen verfügbaren Partitionen auf allen Knoten abgerufen und zusammengeführt werden, was Cassandra aufgrund von schlechter und nicht vorhersagbarer Performance nicht unterstützt. [@DiTullio2016a]

```sql
SELECT * FROM posts
    ORDER BY creation
;
```
: Eine `SELECT` Abfrage mit `ORDER BY` Klausel in SQL {#lst:query-oderby-whereless}

Außerdem unterstützt die CQL keinerlei Joins, wie in [Quellcode @lst:query-join] dargestellt wäre [@CQLManipulation2016].
Der Grund hierfür ist, dass Cassandra konzeptionell annimmt, dass die Daten relationslos, d.h. komplett denormalisiert, sind. Falls weitere Relationen zwischen Datensätzen, die je als eine einzelne Tabelle abgelegt sind, benötigt werden, sollen diese auf Ebene der Applikation realisiert werden [@DataStax2016].

```sql
SELECT * FROM posts
    JOIN ratings ON posts.post_id = ratings.post_id
;
```
: Eine `SELECT` Abfrage mit `JOIN` Klausel in SQL {#lst:query-join}


## Einsatz von Spark als Verstärkung

Spark ist eine passgenaue Lösung, welche diese Funktionen für Cassandra verfügbar macht. Zunächst ist dafür zu beachten, dass sowohl Spark und Cassandra, wie zuvor dargestellt, komplett auf verteiltes Arbeiten ausgerichtet sind. Sie nutzen beide verteilte Systeme zu ihrem Vorteil und sind auf das vernetzte Arbeiten ausgerichtet.

Dadurch können sie optimalerweise gemeinsam auf Knoten verteilt werden, wie in [Abbildung @fig:Cassanda-Spark-Clustering] dargestellt ist. Hierbei teilen sich je ein Spark Worker und ein Cassandra Knoten eine physische Maschine und deren Ressourcen, insbesondere den Speicher [@DiTullio2016].
Wenn Spark nun auf Daten, die von Cassandra bereitgestellt werden, arbeitet, sind die Spark Worker der Verteilung der Daten zwischen den Cassandra Knoten bewusst. Daraus folgt, dass ein Spark Worker nur auf den Daten des Cassandra Knoten auf der selben, lokalen Maschine arbeitet, was zu keinerlei Netzwerkverkehr während der Bearbeitung führt. Daraus folgt eine schnellere Bearbeitungszeit und weniger Fehlerquellen [@Borsos2017].

![Knoten-Struktur für eine Kombination aus Cassandra und Spark](images/Cassanda-Spark-Clustering.png){#fig:Cassanda-Spark-Clustering width=83%}

\pagebreak

Spark und Cassandra arbeiten auch gut miteinander zusammen, wenn ihre Cluster getrennt sind, der eben erläuterte Vorteil kann dann aber nicht voll genutzt werden.

Des Weiteren ist die Relevanz der RDDs zu betrachten, welche im vorherigen Kapitel detailliert erklärt wurden. Cassandra und RDDs verhalten sich in Kernpunkten gleich: Sie arbeiten beide auf dem Konzept der Daten-Partitionierung und sind zeilenorientiert. [@Borsos2017]. Um sie optimal zusammen einzusetzen, ist allerdings der Spark Cassandra Connector nötig, der im Folgenden erläutert wird.

Davor betrachten wir allerdings Alternativen zu Cassandra um Spark mit Daten zu versorgen.

Zunächst ist, aufgrund dessen, dass Spark als Nachfolger und in Anlehnung an Hadoop entwickelt ist, das HDFS zu betrachten. Dies ist das native Dateisystem von Hadoop und daher nativ und vollkommen von Spark unterstützt. Neben mehr Funktionen gegenüber HDFS bietet Cassandra ach ein einfacheres Deployment an. Weiterhin ist Cassandra's Störungsresistenz einfacher und zuverlässiger einzurichten, insbesondere dadurch dass alle Knoten gleich sind und daher gleich verwaltet und gesichert werden können.[@DataStax2013]
Der Hauptpunkt ist allerdings, dass die Daten über Cassandra in einem strukturierten Datenbank-System abgelegt und abgerufen werden, während HDFS als Blob-Speicher nur Dateien annimmt. Daher ist es dort nötig z.B. eine CSV- oder sonstige Klartext-Datei als Quelle und Ziel der Analyse-Daten zu nutzen.[@Borsos2017]

Außerdem kann Spark auch problemlos mit relationalen DBMS zusammenarbeiten. Diese haben allerdings meist eine niedrigere Performance für Daten-Analysen, insbesondere da sie nicht den vorhergegangenen Vorteil der Koordinierung zwischen Spark Workers und Cassandra Knoten beim Datenzugriff nutzen können. Zwar können sie durch den Einsatz entsprechend optimierter Konnektoren annähernd performant wie Cassandra eingebunden werden, sind aber klassischerweise schwer horizontal, d.h. auf mehrere Knoten, skalierbar. [@Borsos2017]

## Spark Cassandra Connector
Der Spark Cassandra Connector ist dafür verantwortlich, die vorher genannte Koordinierung zwischen Spark Workern und Cassandra Knoten zu realisieren. Er macht die Daten dem anwendungsspezifischen Spark-Code nutzbar.
Dafür stellt er Umwandlungen in beide Richtungen zur Verfügung: Daten können aus Cassandra in Spark RDDs geladen werden und aus RDDs implizit in Cassandra Tabellen über ihn gespeichert werden. RDDs werden bei Erstellung außerdem automatisch entsprechend der Partitionierung der Cassandra Repliken aufgeteilt. [@DataStax2018]
Hierfür setzt der Spark Cassandra Connector sowohl manuell definierte Spark SQL Abfragen als auch Aufrufe über Spark DataFrames oder DataSets in entsprechende CQL Abfragen um [@DataStax2018].

\pagebreak

Während der Ausführung rufen die Spark Worker nun den CassandraConnector auf. Dieser baut zunächst partitionierte Spark RDDs entsprechend der auf dem Knoten verfügbaren Daten auf. Die Daten können, durch Einstellungen von Umgebungsvariablen, auch auf mehrere Partitionen aufgeteilt werden und stellen je die Abschnitte des Token-Ringes, die vom Cassandra Knoten verwaltet werden, dar. [@DataStaxAcademy2017]
Daraufhin lädt Spark diese Partitionen in den Hauptspeicher der Maschine und führt die Transformationen und Aktionen wie in der Applikation definiert auf ihnen aus [@DataStaxAcademy2017].
