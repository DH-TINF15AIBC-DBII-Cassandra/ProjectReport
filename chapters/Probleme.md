# Mögliche Problemquellen und Lösungen

Während der Recherche sind einige Probleme wiederholt aufgelistet worden. Diese werden im Folgenden, zusammen mit Lösungsansätzen, detailliert aufgezählt. Diese betreffen allgemein nur entweder Cassandra oder Spark, das Zusammenspiel der beiden, über den Spark Cassandra Connector, läuft weitestgehend reibungslos oder nur mit Einzelfall-Problemen.


## Cassandra

### Design der Partitionen beim Daten-Import

Die Obergrenze für Cassandra Partitionen liegt zwar bei 2GB je Spalte, allerdings sind 1MB ja Spalte für optimale Performance empfohlen.[@DataStax2018a]

Beim Anwendungsfall von Discord führte dies zu Warnungen beim Import, da in ihrem ursprünglichen Partitions Design (je ein Channel ist eine Partition) Partitionen entstanden die diesen Richtwert weit überschritten (aber dennoch unter der Höchstgrenze lagen).
Sie lösten dies, indem der Partition Key nicht nur den Channel sondern auch einen Zeitraum-"Bucket" umfasst. Diese Buckets werden so berechnet, das ja etwa der Nachrichten-Verlauf von zehn Tagen in einem enthalten ist. Somit ist sichergestellt, dass die gesamte Größe der Partitionen je unter den empfohlenen 100MB bleibt.[@Vishnevskiy2017]

Allgemein ist also bei großen Partitionen eine weitere Partitionierung anhand von zusätzlichen Faktoren empfehlenswert.


### Eventuelle Konsistenz

Da Cassandra (in der Standard-Konfiguration) Konsistenz zu Gunsten von Performance einen niedrigeren Stellenwert gibt, können Probleme durch überschriebene Zeilen auftreten.

Im Fall von Discord tritt dies beispielsweise auf, wenn eine Nachricht fast gleichzeitig von einem Benutzer gelöscht und von einem anderen bearbeitet wird. Dies resultiert in einem ungültigen Zustand, indem einige Felder die bearbeiteten Werte, während andere Felder der Zeile einen `NULL` Wert (aufgrund des Löschvorgangs), enthalten.
Um dieses Problem zu umgehen, wurde eine zusätzliche Funktion in die Applikation eingebaut, welche die Tabelle regelmäßig nach solchen ungültigen Zeilen absucht und diese gegebenenfalls komplett löscht.[@Vishnevskiy2017]

Allgemein muss der Effekt der eventuellen Konsistenz beim Design der Applikation, welche die Datenhaltung durch Cassandra nutzt, mit entsprechenden Vorsichts- und Gegenmaßnahmen beachtet werden.


### Löschen von Zeilen und Tombstones

Aufgrund des internen Löschvorgangs für Zeilen, bei dem wie zuvor beschrieben alle Felder mit `NULL` gefüllt und in der SS-Tabelle ein Tombstone hinzugefügt wird, kann der Speicherbedarf einer einzelnen Tabelle schnell anwachsen, wenn häufig Daten gelöscht werden. Diese werden allerdings in bestimmten Intervallen, im \enquote{Compaction} Vorgang, optimiert bzw. gelöscht.

Im Fall von Discord führte dies zu Probleme, aufgrund dessen das in einigen Channels massenhaft Nachrichten erstellt und wieder gelöscht werden. Wenn einer dieser Channel nun geladen wird, muss trotzdem über alle per Tombstone markierten Zeilen iteriert werden, um zu den aktuellen Zeilen zu gelangen. Dies führt zu Performance-Problemen oder gar zu Abstürzen von Knoten, da deren Speicher während des Ladevorgangs voll lief.
Die einfache Lösung dafür war, den Intervall-Abstand zwischen `Compaction` Vorgängen von zehn auf zwei Tage zu verkürzen, wodurch der Daten-Umfang in einem vertretbaren Umfang bleibt.[@Vishnevskiy2017]


## Spark

### Task not serializable

Diese Fehlermeldung tritt erfahrungsgemäß oft dann auf, wenn Spark versucht einen Programmablauf zu parallelisieren, welcher aber Komponenten enthält, die nicht parallel genutzt werden können.
Ein Beispiel dafür wäre es, Datenbank-Verbindungen im Kontext einer `for`-Schleife aufzubauen. Da dabei unter Umständen mehrere Verbindungen zu der selben Datenbank-Tabelle zur selben Zeit aufgebaut werden würden, weigert sich Spark diese Operationen zu parallelisieren.
Ein weiteres Beispiel wäre, dass eine Klasse, welche das Serialisierungs-Interface nicht implementiert, verwendet wird. In diesem Fall kann die Klasse nicht, via Serialisierung, an die verschiedenen Worker verteilt werden, welche die Aufgabe parallel ausführen würden.

<!-- TODO -->
<!-- LogProperties -->
<!-- Maven/SBT overlapping Packages -> exclude -->
<!--java.io.IOException: Could not locate executable `C:\Programs\hadoop-2.8.1\bin\winutils.exe` in the Hadoop binaries.-> hadoop mission -->

<!-- java.io.IOException: Failed to delete: `C:\Users\PaulBauriegel\AppData\Local\Temp\spark-ae4b2738-50d0-4f5b-85a0-6598a72010c9` -->

<!-- https://stackoverflow.com/questions/24706544/how-do-i-make-the-java-hot-spot-maxpermsize-warning-go-away-when-using-intelli -->

