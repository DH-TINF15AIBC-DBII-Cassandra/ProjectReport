FROM bitnami/minideb:stretch

ENV DEBIAN_FRONTEND=noninteractive
ENV PHANTOM_JS=phantomjs-2.1.1-linux-x86_64
ENV PANDOC=pandoc-2.1.1-1-amd64
ENV PANDOC_VER=2.1.1
ENV CROSSREF=linux-ghc82-pandoc20
ENV CROSSREF_VER=v0.3.0.1
ENV TEXLIVE=2017
  
# Install Dependencies
RUN apt-get -qq update
RUN apt-get -qq install wget curl \
	libfreetype6 libfreetype6-dev libfontconfig1 libfontconfig1-dev xzdec \
	lmodern texlive-luatex texlive-latex-base texlive-latex-recommended texlive-math-extra texlive-fonts-recommended

# PhantomJS
RUN wget -nv https://bitbucket.org/ariya/phantomjs/downloads/$PHANTOM_JS.tar.bz2
RUN tar xjf $PHANTOM_JS.tar.bz2 && mv $PHANTOM_JS /usr/local/share  
RUN ln -sf /usr/local/share/$PHANTOM_JS/bin/phantomjs /usr/local/bin

# Pandoc
RUN wget -nv https://github.com/jgm/pandoc/releases/download/$PANDOC_VER/$PANDOC.deb 
RUN dpkg -i $PANDOC.deb                      

# Pandoc Crossref
RUN wget -nv https://github.com/lierdakil/pandoc-crossref/releases/download/$CROSSREF_VER/linux-ghc82-pandoc20.tar.gz                   
RUN tar -xzf linux-ghc82-pandoc20.tar.gz                  
RUN mv -u pandoc-crossref /usr/bin/pandoc-crossref

# Install lonely LaTeX packages
RUN tlmgr init-usertree                          
RUN tlmgr option repository ftp://ftp.math.utah.edu/pub/tex/historic/systems/texlive/$TEXLIVE/tlnet-final/
RUN tlmgr --usermode install xunicode filehook polyglossia etoolbox makecmds \
	pagecolor csquotes mdframed needspace sourcesanspro sourcecodepro titling \
	euenc tipa xcolor acronym floatrow

