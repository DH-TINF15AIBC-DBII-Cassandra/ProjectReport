#!/bin/bash

echo "Building PDF File"
rm _combined.md
(echo --- && cat templates/settings.yml && echo ---) > _combined.md
for chapter in $(cat toc.txt); do 
	(echo && echo && cat chapters/$chapter) >> _combined.md
done
pandoc --from=markdown+hard_line_breaks+backtick_code_blocks+abbreviations --top-level-division=chapter --template=./templates/eisvogel.tex --pdf-engine=lualatex --filter=pandoc-crossref --filter=pandoc-citeproc --listings _combined.md -o ProjectReport.pdf 
open ./ProjectReport.pdf  > check/Missing_Cite.txt

echo "Spellchecking file"
./check/test.sh