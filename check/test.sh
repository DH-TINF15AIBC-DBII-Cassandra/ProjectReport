#!/bin/bash

# Init Script
echo "# Document Issues" > Spelling_Errors.md
echo "## Missing References" >> Spelling_Errors.md
cat check/Missing_Cite.txt >> Spelling_Errors.md
echo "## Spelling Errors" >> Spelling_Errors.md


for dir in chapters/*.md; do
	mkdir tmp

	echo "Parsing $dir"
	grep -Pzo '(?s)```.*?```' $dir > tmp/repl.txt # Find matches
	echo "````" >> tmp/repl.txt				    # Add ```` match exception
	sed 's/\x0/\n/g' tmp/repl.txt > tmp/tt.txt  # write matches into lines

	echo '' > tmp/_combined_prof.md			        # Init proofreading file
	while read -r line; do
		isit=false
		while read -r rep; do
			if [ "$line" == "$rep" ]; then
				isit=true
				break
			fi
		done < tmp/tt.txt
		if [ "$isit" == "false" ] ; then
			echo "$line" >> tmp/_combined_prof.md
		else
			echo "" >> tmp/_combined_prof.md
		fi
	done < $dir

	
	# Del Cites/refs
	sed 's/\[@[a-z=-9-]*\]\|\[@\w*\;@\w*]\|\[@\w*\;@\w*;@\w*]\|\[@\w*\;@\w*\;@\w*\;@\w*]\|\[@\w*;\s@\w*\]\|\[-@\w*:[a-z-]*\]//i' \
	tmp/_combined_prof.md > tmp/text_1	
	echo "test"
	# Del img
	sed -r 's/\!\[(.*)\]\(.*\)\{.*\}/\1/' tmp/text_1 > tmp/_combined_prof.md
	sed -r 's/\!\[(.*)\]\[.*\]/\1/' tmp/_combined_prof.md > tmp/text_1	
	sed -r 's/\[(.*)\]\:.*//' tmp/text_1 > tmp/_combined_prof.md
	# Del links
	sed -r 's/\[(.*)\]\(.*\)//' tmp/_combined_prof.md > tmp/text_1
	# HTML Img-Tag
	sed -r 's/:\s(.*)\{.*\}/\1/' tmp/text_1 > tmp/_combined_prof.md	

	# mv tmp/text_1	 tmp/_combined_prof.md

	echo "Starting Hunspell for $dir"
	echo "### $dir" >> Spelling_Errors.md
	hunspell -L -p check/custom_dic -d en_US,de_DE tmp/_combined_prof.md > tmp/spell
	while read -r line; do
		(echo "$line" && (echo "$line" | hunspell -a -p check/custom_dic -d en_US,de_DE | grep '&' | tr "& " "> ") && echo) >> Spelling_Errors.md
	done < tmp/spell
	
	rm -r tmp
	#break
done