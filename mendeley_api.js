/*
 * Request Mendeley API Key
 *
 * Script structure copied from https://stackoverflow.com/questions/25288307/phantomjs-and-clicking-a-form-button
 * Pass function into evaluate for callback not working, no callback over page loading
 * Not ES6 syntax supported by PhantomJS 
*/

var page = require('webpage').create();
var env = require('system').env;
var email = env['MENDELEY_USER']
var pass = env['MENDELEY_PASS']
var loadInProgress = false;
var testindex = 0;


// Route "console.log()" calls from within the Page context to the main Phantom context (i.e. current "this")
/*page.onConsoleMessage = function(msg) {
    console.log(msg);
};*/
/*
page.onAlert = function(msg) {
    console.log('alert!!> ' + msg);
};*/


page.onLoadStarted = function() {
    loadInProgress = true;
};

page.onLoadFinished = function(status) {
    loadInProgress = false;
    if (status !== 'success') {
        console.log('ERROR: Unable to access network');
        phantom.exit();
    }
};

var steps = [
    function() {
        page.open('https://mendeley-show-me-access-tokens.herokuapp.com/login', function (status) {
		  	page.evaluate(function(email, pass) {
				function insert_credentials(submit_login, email, pass){
					document.evaluate("//input[@name='username']",document,null,XPathResult.ORDERED_NODE_ITERATOR_TYPE,null).iterateNext().value = email;
					document.evaluate("//input[@name='password']",document,null,XPathResult.ORDERED_NODE_ITERATOR_TYPE,null).iterateNext().value = pass;
					submit_login();
				};
				submit_login = function(){
					//page.sendEvent('keypress', page.event.key.Enter);
					document.evaluate("//button[@type='submit']",document,null,XPathResult.ORDERED_NODE_ITERATOR_TYPE,null).iterateNext().click();
				}
				insert_credentials(submit_login, email, pass);
			}, email, pass);
		});
    },
	function() {
		console.log(page.evaluate(function(path) {
			return document.evaluate(path,document,null,XPathResult.ORDERED_NODE_ITERATOR_TYPE,null).iterateNext().innerHTML
		}, "//div[@id='oauth']/dl/dd[1]"));
	}
];

setInterval(function() {
    if (!loadInProgress && typeof steps[testindex] == "function") {
        steps[testindex]();
        testindex++;
    }
    if (typeof steps[testindex] != "function") {
        phantom.exit();
    }
}, 50);