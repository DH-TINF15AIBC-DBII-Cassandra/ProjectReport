# Project Report for Analytics with Cassandra and Spark



[![pipeline status](https://gitlab.com/DH-TINF15AIBC-DBII-Cassandra/ProjectReport/badges/master/pipeline.svg)](https://gitlab.com/DH-TINF15AIBC-DBII-Cassandra/ProjectReport/commits/master) [Build :package:](https://gitlab.com/DH-TINF15AIBC-DBII-Cassandra/ProjectReport/builds/artifacts/master/browse?job=build_pdf)

### Setup
1. Install [Pandoc](https://github.com/jgm/pandoc/releases/tag/2.1.3)(Windows : -> `.msi`; macOS : -> `.pkg`)
2. Download [pandoc-crossref](https://github.com/lierdakil/pandoc-crossref/releases/tag/v0.3.0.2) (Windows: -> `windows-...zip`; macOS: -> `osx-...tar.gz`), extract it and place the `.exe` in the same path as Pandoc
3. Bibliography synced by [Mendeley Desktop](https://www.mendeley.com/download-desktop/) to `DW_Cassandra_Spark.bib` file
    A. Manuell export ("File" -> "Export") 
    B. Activate automatic syncing ("Settings" -> "BibTeX" )
        - "Enable BibTeX syncing" -> "Create one BibTeX file per group" 
        - Path = This Directory
        - Disable "Escape LaTeX special characters.."
	C. Download from last build [:-)](https://gitlab.com/DH-TINF15AIBC-DBII-Cassandra/ProjectReport/-/jobs/62039762/artifacts/file/DW_Cassandra_Spark.bib)


### Building
Windows:
``` cmd
.\compile.bat
```

macOS:
``` bash
./compile.sh
```


### Reference
- [Pandoc’s Markdown](http://pandoc.org/MANUAL.html#pandocs-markdown)
- [Crossref Manual](https://github.com/lierdakil/pandoc-crossref/blob/master/docs/index.md)
